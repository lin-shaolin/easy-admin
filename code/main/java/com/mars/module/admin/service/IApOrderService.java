package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApOrder;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApOrderRequest;
import java.util.List;

/**
 * 订单接口
 *
 * @author mars
 * @date 2023-11-16
 */
public interface IApOrderService {
    /**
     * 新增
     *
     * @param param param
     * @return ApOrder
     */
    ApOrder add(ApOrderRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Integer id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Integer> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApOrderRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApOrder
     */
    ApOrder getById(Integer id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApOrder>
     */
    PageInfo<ApOrder> pageList(ApOrderRequest param);

}
