package com.mars.module.admin.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.util.Date;


/**
 * 订单对象 ap_order
 *
 * @author mars
 * @date 2023-11-16
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "订单对象")
@Accessors(chain = true)
@TableName("ap_order")
public class ApOrder {



    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**
     * 订单号
     */
    @Excel(name = "订单号")
    @ApiModelProperty(value = "订单号")
    private String orderNumber;

    /**
     * user_id
     */
    @Excel(name = "user_id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "user_id")
    private Long userId;

    /**
     * 商品id
     */
    @Excel(name = "商品id")
    @ApiModelProperty(value = "商品id")
    private Integer productId;

    /**
     * 支付时间
     */
    @Excel(name = "支付时间" , width = 30, databaseFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "支付时间")
    private Date paymentTime;

    /**
     * 金额
     */
    @Excel(name = "金额")
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    /**
     * 状态
     */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private Integer state;

    /**
     * 说明
     */
    @Excel(name = "说明")
    @ApiModelProperty(value = "说明")
    private String instructions;

    /**
     * 创建人账号
     */
    @ApiModelProperty(value = "创建人账号")
    private Long createById;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 创建人名称
     */
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 更新人账号
     */
    @ApiModelProperty(value = "更新人账号")
    private Long updateById;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 更新人名称
     */
    @ApiModelProperty(value = "更新人名称")
    private String updateByName;

    /**
     * 逻辑删除
     */
    @ApiModelProperty(value = "逻辑删除")
    private Integer deleted;
}
