package com.mars.module.admin.controller;


import java.util.Arrays;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApOrderService;
import com.mars.module.admin.request.ApOrderRequest;

/**
 * 订单控制层
 *
 * @author mars
 * @date 2023-11-16
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "订单接口管理",tags = "订单接口管理")
@RequestMapping("/admin/apOrder" )
public class ApOrderController {

    private final IApOrderService iApOrderService;

    /**
     * 分页查询订单列表
     */
    @ApiOperation(value = "分页查询订单列表")
    @PostMapping("/pageList")
    public R list(@RequestBody ApOrderRequest apOrder) {
        return R.success(iApOrderService.pageList(apOrder));
    }

    /**
     * 获取订单详细信息
     */
    @ApiOperation(value = "获取订单详细信息")
    @GetMapping(value = "/query/{id}" )
    public R getInfo(@PathVariable("id" ) Integer id) {
        return R.success(iApOrderService.getById(id));
    }

    /**
     * 新增订单
     */
    @ApiOperation(value = "新增订单")
    @PostMapping("/add")
    public R add(@RequestBody ApOrderRequest apOrder) {
        iApOrderService.add(apOrder);
        return R.success();
    }

    /**
     * 修改订单
     */
    @ApiOperation(value = "修改订单")
    @PostMapping("/update")
    public R edit(@RequestBody ApOrderRequest apOrder) {
        iApOrderService.update(apOrder);
        return R.success();
    }

    /**
     * 删除订单
     */
    @ApiOperation(value = "删除订单")
    @PostMapping("/delete/{ids}" )
    public R remove(@PathVariable Integer[] ids) {
        iApOrderService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
