package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApOrder;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 订单Mapper接口
 *
 * @author mars
 * @date 2023-11-16
 */
public interface ApOrderMapper extends BasePlusMapper<ApOrder> {

}
