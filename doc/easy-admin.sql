/*
 Navicat Premium Data Transfer

 Source Server         : 47.98.194.238
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 47.98.194.238:3306
 Source Schema         : easy-admin

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 14/11/2023 23:15:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ap_team
-- ----------------------------
DROP TABLE IF EXISTS `ap_team`;
CREATE TABLE `ap_team`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `number` int NULL DEFAULT NULL COMMENT '人数',
  `introduce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '介绍',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '战队' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ap_team
-- ----------------------------
INSERT INTO `ap_team` VALUES (1, '超级战队', 223, '这是战队这是战队这是战队这是战队这是战队这是战队这是战队');

-- ----------------------------
-- Table structure for ap_user
-- ----------------------------
DROP TABLE IF EXISTS `ap_user`;
CREATE TABLE `ap_user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'open_id',
  `gender` int NULL DEFAULT NULL COMMENT '性别',
  `head_portrait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `if_the_member` int NULL DEFAULT NULL COMMENT '是否会员',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ap_user
-- ----------------------------

-- ----------------------------
-- Table structure for class_info
-- ----------------------------
DROP TABLE IF EXISTS `class_info`;
CREATE TABLE `class_info`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `professional_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '专业名称',
  `in_schoole_time` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '入校年份',
  `department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '系名称',
  `number` int NULL DEFAULT NULL COMMENT '人数',
  `department_no` int NULL DEFAULT NULL COMMENT '系号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '班级表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of class_info
-- ----------------------------
INSERT INTO `class_info` VALUES (6, '121122', '23223', '2323', 1212, 2323);

-- ----------------------------
-- Table structure for gen_create_table_record
-- ----------------------------
DROP TABLE IF EXISTS `gen_create_table_record`;
CREATE TABLE `gen_create_table_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '中文名称',
  `table_en_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '英文名称',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171489978102841345 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '建表记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_create_table_record
-- ----------------------------
INSERT INTO `gen_create_table_record` VALUES (1171489978102841345, '战队', 't_team', 1000000000000000001, '2023-11-14 22:36:38', 'admin', NULL, '2023-11-14 22:36:37', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841346, '战队', 'ap_team', 1000000000000000001, '2023-11-14 22:45:12', 'admin', NULL, '2023-11-14 22:45:11', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841347, '战队', 'ap_team', 1000000000000000001, '2023-11-14 22:50:54', 'admin', NULL, '2023-11-14 22:50:54', NULL, 0);
INSERT INTO `gen_create_table_record` VALUES (1171489978102841348, '用户表', 'ap_user', 1000000000000000001, '2023-11-14 23:00:03', 'admin', NULL, '2023-11-14 23:00:03', NULL, 0);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_menu_type` tinyint NULL DEFAULT NULL COMMENT '1 目录 2 菜单',
  `gen_parent_menu_id` bigint NULL DEFAULT NULL COMMENT '生成的父级菜单id',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `create_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新者',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (114, 'ap_user', '用户表', 'ApUser', 'crud', 'com.mars.module.admin', 'admin', 'user', '用户', 'mars', NULL, NULL, NULL, 1000000000000000001, 'admin', '2023-11-14 23:00:03', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `create_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新者id',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1042 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1082, '114', 'id', '主键id', 'int', 'Integer', 'id', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 1, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1083, '114', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1084, '114', 'open_id', 'open_id', 'varchar(255)', 'String', 'openId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 3, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1085, '114', 'gender', '性别', 'int', 'Integer', 'gender', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 4, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1086, '114', 'head_portrait', '头像', 'varchar(255)', 'String', 'headPortrait', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 5, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1087, '114', 'address', '地址', 'varchar(255)', 'String', 'address', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 6, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1088, '114', 'if_the_member', '是否会员', 'int', 'Integer', 'ifTheMember', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 7, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1089, '114', 'create_by_id', '创建人账号', 'bigint', 'Long', 'createById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 8, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1090, '114', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, NULL, 'datetime', NULL, 9, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1091, '114', 'create_by_name', '创建人名称', 'varchar(255)', 'String', 'createByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 10, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1092, '114', 'update_by_id', '更新人账号', 'bigint', 'Long', 'updateById', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 11, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1093, '114', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, '1', NULL, NULL, NULL, 'datetime', NULL, 12, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1094, '114', 'update_by_name', '更新人名称', 'varchar(100)', 'String', 'updateByName', '0', '0', NULL, NULL, NULL, NULL, NULL, 'LIKE', 'input', NULL, 13, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1095, '114', 'deleted', '逻辑删除', 'tinyint(1)', 'Integer', 'deleted', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, 'input', NULL, 14, 1000000000000000001, 'admin', '2023-11-14 23:10:56', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for resource_info
-- ----------------------------
DROP TABLE IF EXISTS `resource_info`;
CREATE TABLE `resource_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `business_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` tinyint NULL DEFAULT NULL,
  `business_time` datetime NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `business_price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `contacts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5591 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '资源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resource_info
-- ----------------------------
INSERT INTO `resource_info` VALUES (321, '2间箱式房山东荷泽巨野', 10, '2023-10-21 13:55:25', '18865082858', '29', '山东/荷泽/巨野县', '先生');
INSERT INTO `resource_info` VALUES (322, '6个集装箱房四川阿坝', 10, '2023-10-21 08:45:35', '17882470742', '69', '四川/阿坝藏族羌族自治州', '先生');
INSERT INTO `resource_info` VALUES (323, '3间箱式房租一年山东威海', 10, '2023-10-20 15:12:19', '18827514975', '19', '山东/威海', '先生');
INSERT INTO `resource_info` VALUES (324, '采购集装箱卫生间江苏无锡', 10, '2023-10-19 16:37:24', '18112518296', '19', '江苏/无锡', '先生');
INSERT INTO `resource_info` VALUES (325, '十几个3X6箱式房做仓库江苏徐州', 10, '2023-10-19 16:35:15', '13626164105', '129', '江苏/徐州', '女士');
INSERT INTO `resource_info` VALUES (326, '一个5x7米集装箱房山东济宁', 10, '2023-10-19 14:11:27', '15949878233', '19', '山东/济宁', '先生');
INSERT INTO `resource_info` VALUES (327, '3间二手活动板房江苏徐州睢宁县', 10, '2023-10-18 16:44:11', '15305202758', '9', '江苏/徐州/睢宁县', '先生');
INSERT INTO `resource_info` VALUES (328, '求租2间箱式房用3-4个月四川南充', 10, '2023-10-18 16:25:26', '18989191612', '19', '四川/南充', '先生');
INSERT INTO `resource_info` VALUES (329, '求租2间箱式房用到年后江苏苏州昆山', 10, '2023-10-18 16:18:39', '15050288027', '19', '江苏/苏州/昆山', '女士');
INSERT INTO `resource_info` VALUES (330, '一个4X10米集装箱房江苏南通启东', 10, '2023-10-18 09:02:32', '15862834186', '19', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (331, '两个集装箱房租期8个月江苏淮安', 10, '2023-10-18 08:54:14', '15189539666', '19', '江苏/淮安', '先生');
INSERT INTO `resource_info` VALUES (332, '大型煤矿需要采购大面积活动板板工人已进场项目在丽江附近云南丽江', 10, '2023-10-17 20:27:38', '18865467676', '299', '云南/丽江', '先生');
INSERT INTO `resource_info` VALUES (333, '3间活动板房四川 巴中', 10, '2023-10-17 20:02:50', '18080501100', '19', '四川/巴中', '先生');
INSERT INTO `resource_info` VALUES (334, '集装箱厕所4男位+2女位江苏常州', 10, '2023-10-17 13:37:52', '13815060529', '29', '江苏/常州', '先生');
INSERT INTO `resource_info` VALUES (335, '10X7米活动板房江苏宿迁沐阳', 10, '2023-10-17 08:31:57', '15240390456', '19', '江苏/宿迁/沭阳县', '先生');
INSERT INTO `resource_info` VALUES (336, '10多间打包箱房山东荷泽', 10, '2023-10-16 13:29:57', '17710858666', '99', '山东/荷泽', '先生');
INSERT INTO `resource_info` VALUES (337, '定制一间10×7的箱式房，四川乐山', 10, '2023-10-16 13:26:07', '13438711466', '29', '四川/乐山', '先生');
INSERT INTO `resource_info` VALUES (338, '20间箱式房3X6山东济南章丘', 10, '2023-10-16 10:16:11', '13718598815', '129', '山东/济南/章丘', '先生');
INSERT INTO `resource_info` VALUES (339, '一套卫生间加洗澡间一体的活动板房江苏南通海安', 10, '2023-10-16 08:46:29', '15151300862', '9', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (340, '定制多个种磨菇的集装箱4.5X 3.5在山东和江苏镇江', 10, '2023-10-14 16:46:00', '13775548688', '99', '江苏/镇江', '先生');
INSERT INTO `resource_info` VALUES (1541, '20个6米高的太阳能路灯 湖北-恩施', 15, '2023-10-21 19:56:52', '19986746886', '29', '湖北/恩施土家族苗族自治州', '先生');
INSERT INTO `resource_info` VALUES (1542, '3000多盏6米高的太阳能路村庄里用灯项目已中标前期先要1000多盏工程在云南曲靖会泽县', 15, '2023-10-21 19:40:21', '18788569036', '499', '云南/曲靖/会泽县', '先生');
INSERT INTO `resource_info` VALUES (1543, '10个5米高的太阳能路灯湖南衡阳祁东县', 15, '2023-10-21 19:36:16', '18673418388', '19', '湖南/衡阳/祁东县', '先生');
INSERT INTO `resource_info` VALUES (1544, '10个6米高的太阳能路灯加安装建议当地厂家联系四川达州', 15, '2023-10-21 19:34:21', '15881844196', '19', '四川/达州', '先生');
INSERT INTO `resource_info` VALUES (1545, '30盏6米村庄道路上用的太阳能路灯四川绵阳\n', 15, '2023-10-21 18:52:45', '15908200001', '29', '四川/绵阳', '先生');
INSERT INTO `resource_info` VALUES (1546, '10几盏3-4米高太阳能路灯厂区用的河北邯郸', 15, '2023-10-21 18:45:41', '17732010002', '29', '河北/邯郸', '先生');
INSERT INTO `resource_info` VALUES (1547, '4个4米的市电路灯，要防爆灯头加微信选款四川泸州', 15, '2023-10-21 18:44:13', '15885683973', '19', '四川/泸州/古蔺县', '先生');
INSERT INTO `resource_info` VALUES (1548, '项目采购几百盏4米高太阳能路灯 江西上饶', 15, '2023-10-21 18:43:14', '13870360508', '199', '江西/上饶/鄱阳县', '先生');
INSERT INTO `resource_info` VALUES (1549, '20个6米的太阳能路灯河南周口项城', 15, '2023-10-21 18:40:50', '15886808576', '29', '河南/周口/项城', '先生');
INSERT INTO `resource_info` VALUES (1550, '咨询10盏太阳能路灯6米高四川', 15, '2023-10-21 18:39:03', '13822702358', '9', '四川', '先生');
INSERT INTO `resource_info` VALUES (1551, '生态廊道及周边的亮化工程价格合适包工包料浙江宁波', 15, '2023-10-21 18:16:54', '13362106320', '99', '浙江/宁波', '女士');
INSERT INTO `resource_info` VALUES (1552, '咨询太阳能路灯6公里道理采购只要灯头云南昭通\n', 15, '2023-10-21 17:55:45', '18069915239', '429', '云南/昭通/镇雄县', '先生');
INSERT INTO `resource_info` VALUES (1553, '125米隧道亮化工程对接承接方山西晋中', 15, '2023-10-21 17:45:38', '18568576685', '69', '山西/晋中', '先生');
INSERT INTO `resource_info` VALUES (1554, '工程采购太阳能和市电路灯5米高加微信发图片报价河北邢台', 15, '2023-10-21 17:43:09', '13910558951', '99', '河北/邢台', '先生');
INSERT INTO `resource_info` VALUES (1555, '太阳能路灯9盏3米高广东惠州', 15, '2023-10-21 17:22:55', '13413104374', '19', '广东/惠州', '先生');
INSERT INTO `resource_info` VALUES (1556, '10个6米高太阳能路灯湖南衡阳', 15, '2023-10-21 16:58:42', '15173479359', '29', '湖南/衡阳', '先生');
INSERT INTO `resource_info` VALUES (1557, '太阳能路灯1盏5米高陕西宝鸡', 15, '2023-10-21 16:38:17', '18992705835', '9', '陕西/宝鸡/凤翔县', '先生');
INSERT INTO `resource_info` VALUES (1558, '两个5米高太阳能路灯甘肃白银', 15, '2023-10-21 13:32:04', '18119481416', '9', '甘肃/白银', '先生');
INSERT INTO `resource_info` VALUES (1559, '20盏6米高的太阳能路灯甘肃平凉', 15, '2023-10-21 13:31:20', '13830019566', '29', '甘肃/平凉', '先生');
INSERT INTO `resource_info` VALUES (1560, '采购做亮化工程用的灯带要一线品牌，价格实惠些的加微信河北沧州黄骅', 15, '2023-10-21 13:24:04', '17772677510', '99', '河北/沧州/黄骅', '女士');
INSERT INTO `resource_info` VALUES (1561, '30个太阳能路灯不需要灯杆云南德宏', 15, '2023-10-21 11:02:40', '15308827171', '29', '云南/德宏傣族景颇族自治州', '先生');
INSERT INTO `resource_info` VALUES (1562, '三次咨询太阳能路灯10盏5米高四川巴中', 15, '2023-10-21 10:37:52', '15109472989', '19', '四川/巴中/南江县', '先生');
INSERT INTO `resource_info` VALUES (1563, '二次来电8000万灯光秀项目对接总包方有图纸重庆', 15, '2023-10-21 10:33:50', '18696695155', '499', '重庆', '先生');
INSERT INTO `resource_info` VALUES (1564, '8个6米高太阳能和市电路灯内蒙古兴安', 15, '2023-10-21 09:52:28', '15849806699', '19', '内蒙古/兴安盟', '先生');
INSERT INTO `resource_info` VALUES (1565, '200个厂房仓库用防爆灯山东临沂郯城', 15, '2023-10-21 09:23:32', '13601899455', '159', '山东/临沂/郯城县', '先生');
INSERT INTO `resource_info` VALUES (1566, '太阳能路灯50盏6米高80瓦山西朔州\n', 15, '2023-10-21 09:12:16', '18634922228', '69', '山西/朔州', '先生');
INSERT INTO `resource_info` VALUES (1567, '一条商业街亮化工程设计材料加施，客户要求到现场实地规划面谈贵州贵阳', 15, '2023-10-21 09:06:22', '18641446802', '99', '贵州/贵阳', '先生');
INSERT INTO `resource_info` VALUES (1568, '100个7米高和6米高太阳能路灯江西宜春', 15, '2023-10-21 08:57:43', '13970572177', '159', '江西/宜春', '先生');
INSERT INTO `resource_info` VALUES (1569, '20个太阳能路灯不需灯杆湖南湘西', 15, '2023-10-21 08:41:29', '17752615009', '29', '湖南/湘西土家族苗族自治州', '先生');
INSERT INTO `resource_info` VALUES (1570, '太阳能路灯3万个只要灯头江西上饶', 15, '2023-10-21 08:38:24', '15338092233', '499', '江西/上饶', '先生');
INSERT INTO `resource_info` VALUES (1571, '15个6米高太阳能路灯湖南衡阳衡山', 15, '2023-10-21 08:33:44', '18975214917', '29', '湖南/衡阳/衡山县', '先生');
INSERT INTO `resource_info` VALUES (1572, '太阳能路灯几盏5米高加微信报价江西宜春', 15, '2023-10-21 08:26:39', '15279560297', '9', '江西/宜春', '先生');
INSERT INTO `resource_info` VALUES (1573, '100盏5米高太阳能路灯 广东江门', 15, '2023-10-20 20:54:58', '13824004103', '159', '广东/江门', '先生');
INSERT INTO `resource_info` VALUES (1574, '50-100盏5米高的太阳能路灯重庆', 15, '2023-10-20 19:58:04', '13896320733', '99', '重庆', '先生');
INSERT INTO `resource_info` VALUES (1575, '5盏7米高太阳能路灯 湖南郴州', 15, '2023-10-20 19:55:01', '15073593087', '9', '湖南/郴州', '先生');
INSERT INTO `resource_info` VALUES (1576, '找路灯厂批发太阳能路灯自己是销售的湖北恩施', 15, '2023-10-20 19:42:57', '13924805582', '99', '湖北', '先生');
INSERT INTO `resource_info` VALUES (1577, '3个5米的太阳能路灯湖南-张家界', 15, '2023-10-20 19:20:53', '17711745657', '9', '湖南/张家界', '先生');
INSERT INTO `resource_info` VALUES (1578, '3盏6米高太阳能路灯 江西抚州', 15, '2023-10-20 19:04:01', '18879461888', '9', '江西/抚州', '先生');
INSERT INTO `resource_info` VALUES (1579, '10个5米高的太阳能路灯 江西上饶', 15, '2023-10-20 18:29:53', '13317935258', '19', '江西/上饶', '先生');
INSERT INTO `resource_info` VALUES (1580, '1个亿的城市灯光生态文旅项目找做亮化厂家合作加微信不招标重庆', 15, '2023-10-20 17:41:54', '18696695155', '499', '重庆', '先生');
INSERT INTO `resource_info` VALUES (1581, '太阳能路灯2盏四川自贡', 15, '2023-10-20 17:16:23', '17395373214', '9', '四川/自贡', '先生');
INSERT INTO `resource_info` VALUES (1582, '30盏6米高的太阳能路灯湖南株洲攸县', 15, '2023-10-20 17:03:30', '13077088372', '29', '湖南/株洲/攸县', '先生');
INSERT INTO `resource_info` VALUES (1583, '2个500米过街天桥的亮化用的灯带另咨询是否能安装建议当地厂家联系青海西宁', 15, '2023-10-20 17:00:31', '13639783776', '49', '青海/西宁', '先生');
INSERT INTO `resource_info` VALUES (1584, '1000平商场内部亮化工程直接对接商场可周一到现场面谈贵州贵阳', 15, '2023-10-20 16:56:03', '17785117625', '99', '贵州/贵阳', '先生');
INSERT INTO `resource_info` VALUES (1585, '200盏9米高中华灯含安装吉林延吉', 15, '2023-10-20 16:14:52', '19997197778', '299', '吉林/延边朝鲜族自治州/延吉', '先生');
INSERT INTO `resource_info` VALUES (1586, '700个6米高乡村太阳能路灯河北石家庄', 15, '2023-10-20 16:04:16', '18131031166', '399', '河北/石家庄', '先生');
INSERT INTO `resource_info` VALUES (1587, '50-100套6米的太阳能路灯河北承德', 15, '2023-10-20 15:36:46', '17320986189', '99', '河北/承德', '先生');
INSERT INTO `resource_info` VALUES (1588, '30个5米高太阳能路灯100W陕西宝鸡', 15, '2023-10-20 15:11:25', '13359175298', '29', '陕西/宝鸡', '先生');
INSERT INTO `resource_info` VALUES (1589, ' 太阳能路灯30-40盏6米高广东韶关', 15, '2023-10-20 14:28:15', '18420112321', '49', '广东/韶关', '先生');
INSERT INTO `resource_info` VALUES (1590, '想找太阳能灯厂家采购庭院用太阳能灯自己是开店的加微信云南曲靖', 15, '2023-10-20 11:44:07', '13887480294', '99', '云南/曲靖', '先生');
INSERT INTO `resource_info` VALUES (1691, '700平办公室做环氧地坪包工包料浙江杭州', 23, '2023-10-21 20:31:41', '15868892020', '49', '浙江/杭州', '先生');
INSERT INTO `resource_info` VALUES (1692, '2万平+3000平+7000平车库做环氧地坪包工包料对接甲方不需要投标明天早上回电话 黑龙江双鸭山宝清县', 23, '2023-10-21 20:05:57', '13634696224', '599', '黑龙江/双鸭山/宝清县', '先生');
INSERT INTO `resource_info` VALUES (1693, '咨询家装300平环氧地坪山西临汾霍州', 23, '2023-10-21 19:26:05', '13323621552', '19', '山西/临汾/霍州', '先生');
INSERT INTO `resource_info` VALUES (1694, '2000平厂房做环氧地坪包工包料对接装修公司广东东莞', 23, '2023-10-21 19:22:55', '13690705668', '129', '广东/东莞', '先生');
INSERT INTO `resource_info` VALUES (1695, '1万平厂房做固化地坪包工包料对接业主不需要投标浙江临平', 23, '2023-10-21 19:19:36', '13506737648', '399', '浙江/杭州/余杭区', '先生');
INSERT INTO `resource_info` VALUES (1696, '3000平方厂区固化地坪包工包料或包工分开报价对接业主四川成都', 23, '2023-10-21 18:50:50', '17764982216', '159', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (1697, '咨询500平厂区环氧地坪广东东莞', 23, '2023-10-21 18:27:25', '18929100616', '19', '广东/东莞', '女士');
INSERT INTO `resource_info` VALUES (1698, '600平厂房做环氧地坪包工包料对接业主 湖南常德', 23, '2023-10-21 18:27:04', '13875092730', '49', '湖南/常德', '先生');
INSERT INTO `resource_info` VALUES (1699, '260平学校做环氧地坪包工包料对接负责人 四川凉山', 23, '2023-10-21 18:24:20', '18384493247', '29', '四川/凉山彝族自治州', '女士');
INSERT INTO `resource_info` VALUES (1700, '汽修厂环氧地坪200多平包工包料河北秦皇岛', 23, '2023-10-21 17:50:14', '13933557734', '29', '河北/秦皇岛', '女士');
INSERT INTO `resource_info` VALUES (1701, '300平家装环氧地坪 江苏徐州', 23, '2023-10-21 17:43:07', '13952271017', '29', '江苏/徐州/邳州', '先生');
INSERT INTO `resource_info` VALUES (1702, '7平设备间环氧地坪整包先看效果个人做福建龙岩长汀', 23, '2023-10-21 17:39:56', '18159858971', '9', '福建/龙岩/长汀县', '先生');
INSERT INTO `resource_info` VALUES (1703, '环氧自流平4000多平包工包料浙江绍兴', 23, '2023-10-21 17:31:27', '19941462097', '209', '浙江/绍兴', '先生');
INSERT INTO `resource_info` VALUES (1704, '车间环氧地坪400平包工包料江西南昌\n', 23, '2023-10-21 17:28:49', '13133823680', '39', '江西/南昌', '先生');
INSERT INTO `resource_info` VALUES (1705, '700-800平环氧地坪包工包料对接业主 福建泉州', 23, '2023-10-21 17:21:01', '15159585561', '49', '福建/泉州', '先生');
INSERT INTO `resource_info` VALUES (1706, '300平仓库环氧地坪包工包料山西-运城', 23, '2023-10-21 17:15:26', '13467288088', '29', '山西/运城', '先生');
INSERT INTO `resource_info` VALUES (1707, '聚氨酯砂浆地坪先在100多平样板包工包料新疆乌鲁木齐', 23, '2023-10-21 17:01:46', '13629946555', '19', '新疆/乌鲁木齐', '先生');
INSERT INTO `resource_info` VALUES (1708, '600平厂区环氧地坪包工包料四川广元', 23, '2023-10-21 16:44:16', '18113692204', '49', '四川/广元', '先生');
INSERT INTO `resource_info` VALUES (1709, '700-800平办公室做固化地坪包工包料对接业主 浙江金华', 23, '2023-10-21 16:40:40', '13095899992', '49', '浙江/金华', '先生');
INSERT INTO `resource_info` VALUES (1710, '600多平环氧地坪整包与承接人联系湖南株洲炎陵', 23, '2023-10-21 16:34:37', '13874161927', '49', '湖南/株洲/炎陵县', '先生');
INSERT INTO `resource_info` VALUES (1711, '10平机房处理室无尘车间百级标准整包对接装修公司 陕西西安', 18, '2023-10-21 20:29:55', '13720400806', '19', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (1712, '450平酒厂需要规划一下无尘车间面积整备对接业主约时间面谈 新疆昌吉阜康', 18, '2023-10-21 20:11:06', '13999139456', '69', '新疆/昌吉回族自治州/阜康', '先生');
INSERT INTO `resource_info` VALUES (1713, '咨询风淋室和传递窗，自己是做罐装设备的有客户需要贵州', 18, '2023-10-21 20:04:35', '18968253870', '49', '贵州', '先生');
INSERT INTO `resource_info` VALUES (1714, '3000平食品厂无尘车间厂房已建好对接业主需要达到认证标准要走内标黑龙江齐齐哈尔', 18, '2023-10-21 19:53:45', '15663226137', '259', '黑龙江/齐齐哈尔', '先生');
INSERT INTO `resource_info` VALUES (1715, '2万平方电池厂千级无尘车间对接承接方不用招标福建莆田仙游县', 18, '2023-10-21 19:51:11', '13860491533', '699', '福建/莆田/仙游县', '先生');
INSERT INTO `resource_info` VALUES (1716, '二次来电300平食品罐装车间的洁净工程整包对接业主建议本地厂家联系江西南昌', 18, '2023-10-21 19:47:52', '13970094658', '69', '江西', '先生');
INSERT INTO `resource_info` VALUES (1717, '两个30平手术室+1间配药室的洁净工程对接业主可到现场面谈山东枣庄滕州', 18, '2023-10-21 19:43:11', '13272927829', '39', '山东/枣庄/滕州', '女士');
INSERT INTO `resource_info` VALUES (1718, '二次留言 300平食品厂无尘车间整包对接业主加微信发草图新疆乌鲁木齐', 18, '2023-10-21 19:43:08', '15009910500', '69', '新疆/乌鲁木齐', '先生');
INSERT INTO `resource_info` VALUES (1719, '1000平电子废料加工厂无尘车间看上去干净整洁即可重庆-重庆', 18, '2023-10-21 19:37:59', '18680881513', '199', '重庆/江北区/重庆', '先生');
INSERT INTO `resource_info` VALUES (1720, '多次来电600平食品厂无尘车间要分包对接业主分开报价哪项价格合适做哪项山西晋中平遥县', 18, '2023-10-21 19:23:39', '15582873515', '99', '山西/晋中/平遥县', '先生');
INSERT INTO `resource_info` VALUES (1721, '1万平的玻镁净化板+安装对接建设公司加微信河南三门峡', 18, '2023-10-21 18:22:46', '15955107438', '499', '河南/三门峡', '先生');
INSERT INTO `resource_info` VALUES (1722, '500平食品厂无尘车间整包对接业主 江苏盐城大丰', 18, '2023-10-21 18:15:08', '15851014005', '139', '江苏/盐城/大丰', '先生');
INSERT INTO `resource_info` VALUES (1723, '20平洗洁精厂无尘车间整包新疆乌鲁木齐', 18, '2023-10-21 18:12:34', '17501636805', '9', '新疆/乌鲁木齐', '先生');
INSERT INTO `resource_info` VALUES (1724, '塑胶厂无尘车间对接业主加微信可约时间到厂区面谈江苏苏州', 18, '2023-10-21 18:04:22', '18917328679', '99', '江苏/苏州', '先生');
INSERT INTO `resource_info` VALUES (1725, '400平玻镁板整包做吊顶用与承接人联系广东深圳', 18, '2023-10-21 18:00:51', '13322929779', '29', '广东/深圳', '先生');
INSERT INTO `resource_info` VALUES (1726, '食品厂无尘车间2000平只做车间新风系统和净化设备对接业主陕西咸阳', 18, '2023-10-21 17:59:41', '18648332777', '159', '陕西/咸阳/杨凌区', '女士');
INSERT INTO `resource_info` VALUES (1727, '喷漆厂无尘车间130平对接承接方辽宁锦州', 18, '2023-10-21 17:47:19', '18256818855', '39', '辽宁/锦州', '先生');
INSERT INTO `resource_info` VALUES (1728, '100平手术室做净化工程整包对接负责人约明天时间去现场沟通指定当地厂家联系海南三亚', 18, '2023-10-21 17:43:05', '15274902655', '39', '海南/三亚', '先生');
INSERT INTO `resource_info` VALUES (1729, '食品厂无尘车间要求加微信沟通重庆', 18, '2023-10-21 17:23:08', '18717006097', '99', '重庆', '先生');
INSERT INTO `resource_info` VALUES (1730, '4000平食品无尘车间与承接人联系内蒙古通辽', 18, '2023-10-21 17:21:49', '15822886003', '299', '内蒙古/通辽', '先生');
INSERT INTO `resource_info` VALUES (2121, '20盏6米高太阳能路灯四川南充', 0, '2023-10-21 21:12:14', '13551911122', '29', '四川/南充/蓬安县', '先生');
INSERT INTO `resource_info` VALUES (2122, '2.4X8膜结构停车棚加微信沟通山东烟台', 0, '2023-10-21 21:10:10', '18854595828', '9', '山东/烟台', '先生');
INSERT INTO `resource_info` VALUES (2123, '食品厂无尘车间要求赤峰当地厂家联系内蒙赤峰', 0, '2023-10-21 21:09:30', '18147692223', '99', '内蒙古/赤峰', '先生');
INSERT INTO `resource_info` VALUES (2124, '30平方鲜花冷库湖北-恩施', 0, '2023-10-21 21:08:07', '18727755265', '49', '湖北/恩施土家族苗族自治州', '女士');
INSERT INTO `resource_info` VALUES (2125, '600-700平厂房水泥地面做环氧地坪湖南岳阳', 0, '2023-10-21 21:07:15', '18152600886', '49', '湖南/岳阳/平江县', '先生');
INSERT INTO `resource_info` VALUES (2126, '4x6可移动的冷库陕西-西安', 0, '2023-10-21 21:06:37', '18335921927', '39', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (2127, '20平方的冷冻冷库加微信建议当地厂家想到厂区看贵州六盘水\n', 0, '2023-10-21 21:04:35', '18748772666', '39', '贵州', '先生');
INSERT INTO `resource_info` VALUES (2128, '5米长乘14.3米宽电动推拉棚自家做江苏盐城', 0, '2023-10-21 21:04:15', '13291366766', '29', '江苏/盐城', '女士');
INSERT INTO `resource_info` VALUES (2129, '几十平膜结构摩托车停车棚自家做四川南充', 0, '2023-10-21 21:02:07', '18086929157', '29', '四川/南充/蓬安县', '先生');
INSERT INTO `resource_info` VALUES (2130, '咨询300平电动推拉棚河南南阳', 0, '2023-10-21 21:00:34', '18625621891', '49', '河南/南阳', '先生');
INSERT INTO `resource_info` VALUES (2131, '咨询 800平2米高保鲜库个人建 四川广安', 0, '2023-10-21 21:00:20', '13647661761', '199', '四川/广安/武胜县', '先生');
INSERT INTO `resource_info` VALUES (2132, '12X5的膜结构汽车车棚浙江温州', 0, '2023-10-21 20:56:52', '13968939648', '49', '浙江/温州', '先生');
INSERT INTO `resource_info` VALUES (2133, '2.3x2.55x4.2移动式保鲜库江苏盐城', 0, '2023-10-21 20:53:38', '13327992252', '29', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (2134, '30多个汽车车位的膜结构车棚+充电桩加微信广西来宾', 0, '2023-10-21 20:53:14', '13802873617', '159', '广西/来宾', '先生');
INSERT INTO `resource_info` VALUES (2135, '200-300平店面做环氧地坪加微信沟通湖北荆门', 0, '2023-10-21 20:51:33', '13093239994', '29', '湖北/荆门', '先生');
INSERT INTO `resource_info` VALUES (2136, '定制10几个净化门规格有好几种加微信浙江宁波', 0, '2023-10-21 20:50:30', '13044610412', '99', '浙江/宁波/慈溪', '先生');
INSERT INTO `resource_info` VALUES (2137, '二次留言 10-12平自动除霜冷冻库有场地安徽亳州', 0, '2023-10-21 20:49:36', '13339170796', '29', '安徽/亳州', '先生');
INSERT INTO `resource_info` VALUES (2138, '300平膜结构遮阳棚广东广州', 0, '2023-10-21 20:47:42', '13710863656', '99', '广东/广州', '先生');
INSERT INTO `resource_info` VALUES (2139, '1000平4-6米高保鲜库有场地个人建 贵州黔东南', 0, '2023-10-21 20:46:13', '13595531881', '499', '贵州/黔东南苗族侗族自治州', '先生');
INSERT INTO `resource_info` VALUES (2140, '几百盏6米高的太阳能路灯需要招标加微信发资料重庆', 0, '2023-10-21 20:45:34', '15823015278', '199', '重庆', '先生');
INSERT INTO `resource_info` VALUES (2141, '300-400平厂房做环氧地坪包工包料对接承接方 湖南岳阳', 0, '2023-10-21 20:43:15', '18773098168', '39', '湖南/岳阳/平江县', '先生');
INSERT INTO `resource_info` VALUES (2142, '15平方存放果蔬的冷库青海海东', 0, '2023-10-21 20:42:04', '13897697558', '39', '青海/海东地区', '先生');
INSERT INTO `resource_info` VALUES (2143, '400平膜结构停车棚连基础一起做对接承接方加微信发图纸山东济宁', 0, '2023-10-21 20:39:56', '15069766966', '139', '山东/济宁', '先生');
INSERT INTO `resource_info` VALUES (2144, '咨询18平方存放食用菌的冷库江苏淮安', 0, '2023-10-21 20:36:51', '18151479895', '19', '江苏/淮安', '女士');
INSERT INTO `resource_info` VALUES (2145, '700平修理厂做环氧地坪包工包料对接业主 四川成都', 0, '2023-10-21 20:35:53', '13541007422', '49', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (2146, '60平食品包装车间的洁净工程厂房下月竣工湖北黄冈', 0, '2023-10-21 20:34:20', '13264836913', '19', '湖北/黄冈', '先生');
INSERT INTO `resource_info` VALUES (2147, '院子外面做18X8电动推拉棚安徽淮北', 0, '2023-10-21 20:32:32', '13855766249', '49', '安徽/淮北', '先生');
INSERT INTO `resource_info` VALUES (2148, '700平办公室做环氧地坪包工包料浙江杭州', 0, '2023-10-21 20:31:41', '15868892020', '49', '浙江/杭州', '先生');
INSERT INTO `resource_info` VALUES (2149, '10平机房处理室无尘车间百级标准整包对接装修公司 陕西西安', 0, '2023-10-21 20:29:55', '13720400806', '19', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (2150, '客户是做充电桩的一周之后有膜结构停车棚的项目找厂家合作加微信沟通主要在山东济南和青岛', 0, '2023-10-21 20:24:48', '18554372786', '99', '山东/济南', '先生');
INSERT INTO `resource_info` VALUES (2151, '3个车位膜结构车棚 江苏徐州', 0, '2023-10-21 20:23:33', '13236039098', '29', '江苏/徐州', '先生');
INSERT INTO `resource_info` VALUES (2152, '工地上需要采购几间集装箱房和活动板房卫生间新的和二手的均要了解价格浙江宁波', 0, '2023-10-21 20:21:23', '13903665191', '99', '浙江/宁波', '先生');
INSERT INTO `resource_info` VALUES (2153, '6平的冷冻的冷库餐饮店做四川泸州', 0, '2023-10-21 20:13:33', '18380930306', '19', '四川/泸州/泸县', '女士');
INSERT INTO `resource_info` VALUES (2154, '60平方的冷冻冷库厂区里建浙江台州', 0, '2023-10-21 20:11:09', '18267684718', '79', '浙江/台州', '先生');
INSERT INTO `resource_info` VALUES (2155, '450平酒厂需要规划一下无尘车间面积整备对接业主约时间面谈 新疆昌吉阜康', 0, '2023-10-21 20:11:06', '13999139456', '69', '新疆/昌吉回族自治州/阜康', '先生');
INSERT INTO `resource_info` VALUES (2156, '60平冷冻库有场地 浙江台州', 0, '2023-10-21 20:07:46', '13978084718', '79', '浙江/台州', '先生');
INSERT INTO `resource_info` VALUES (2157, '30多平方的零下20度冷库江苏盐城', 0, '2023-10-21 20:06:49', '13024489998', '49', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (2158, '2万平+3000平+7000平车库做环氧地坪包工包料对接甲方不需要投标明天早上回电话 黑龙江双鸭山宝清县', 0, '2023-10-21 20:05:57', '13634696224', '599', '黑龙江/双鸭山/宝清县', '先生');
INSERT INTO `resource_info` VALUES (2159, '咨询风淋室和传递窗，自己是做罐装设备的有客户需要贵州', 0, '2023-10-21 20:04:35', '18968253870', '49', '贵州', '先生');
INSERT INTO `resource_info` VALUES (2160, '20X6膜结构电动车停车棚对接承接方浙江宁波', 0, '2023-10-21 20:01:19', '18658421281', '79', '浙江/宁波', '先生');
INSERT INTO `resource_info` VALUES (2161, '10平方的保鲜冷库蔬菜店做贵州遵义', 0, '2023-10-21 20:00:58', '18076266995', '29', '贵州/遵义/仁怀', '女士');
INSERT INTO `resource_info` VALUES (2162, '20个6米高的太阳能路灯 湖北-恩施', 0, '2023-10-21 19:56:52', '19986746886', '29', '湖北/恩施土家族苗族自治州', '先生');
INSERT INTO `resource_info` VALUES (2163, '2个共计100平左右膜结构电动自行车停车棚对接物业安徽宿州萧县', 0, '2023-10-21 19:56:06', '17087819111', '49', '安徽/宿州/萧县', '先生');
INSERT INTO `resource_info` VALUES (2164, '3000平食品厂无尘车间厂房已建好对接业主需要达到认证标准要走内标黑龙江齐齐哈尔', 0, '2023-10-21 19:53:45', '15663226137', '259', '黑龙江/齐齐哈尔', '先生');
INSERT INTO `resource_info` VALUES (2165, '2万平方电池厂千级无尘车间对接承接方不用招标福建莆田仙游县', 0, '2023-10-21 19:51:11', '13860491533', '699', '福建/莆田/仙游县', '先生');
INSERT INTO `resource_info` VALUES (2166, '20x25x3二手冷冻库有场地个人建 福建宁德福鼎', 0, '2023-10-21 19:50:24', '18750388866', '259', '福建/宁德/福鼎', '先生');
INSERT INTO `resource_info` VALUES (2167, '3个5米长x40公分宽楼梯踏步+10平平铺要将军红石材江苏徐州', 0, '2023-10-21 19:48:16', '13951354009', '9', '江苏/徐州', '先生');
INSERT INTO `resource_info` VALUES (2168, '二次来电300平食品罐装车间的洁净工程整包对接业主建议本地厂家联系江西南昌', 0, '2023-10-21 19:47:52', '13970094658', '69', '江西', '先生');
INSERT INTO `resource_info` VALUES (2169, '两个30平手术室+1间配药室的洁净工程对接业主可到现场面谈山东枣庄滕州', 0, '2023-10-21 19:43:11', '13272927829', '39', '山东/枣庄/滕州', '女士');
INSERT INTO `resource_info` VALUES (2170, '二次留言 300平食品厂无尘车间整包对接业主加微信发草图新疆乌鲁木齐', 0, '2023-10-21 19:43:08', '15009910500', '69', '新疆/乌鲁木齐', '先生');
INSERT INTO `resource_info` VALUES (2481, '3个5米长x40公分宽楼梯踏步+10平平铺要将军红石材江苏徐州', 21, '2023-10-21 19:48:16', '13951354009', '9', '江苏/徐州', '先生');
INSERT INTO `resource_info` VALUES (2482, '做工程的需要芝麻白芝麻黑平铺石材加微信江苏苏州', 21, '2023-10-21 19:19:24', '13914075687', '99', '江苏/苏州', '先生');
INSERT INTO `resource_info` VALUES (2483, '5000米芝麻灰路沿石15X30，长80或90浙江嘉兴', 21, '2023-10-21 18:25:28', '15988331869', '229', '浙江/嘉兴', '先生');
INSERT INTO `resource_info` VALUES (2484, '20平将军红铺地用安徽黄山', 21, '2023-10-21 17:44:18', '13705596283', '9', '安徽/黄山', '先生');
INSERT INTO `resource_info` VALUES (2485, '采购墓碑石自己是做雕刻的安徽安庆', 21, '2023-10-21 17:32:38', '13866006810', '49', '安徽/安庆/宿松县', '先生');
INSERT INTO `resource_info` VALUES (2486, '芝麻灰几十平1.8厚300乘600河南洛阳', 21, '2023-10-21 17:19:37', '13526961138', '9', '河南/洛阳', '先生');
INSERT INTO `resource_info` VALUES (2487, '300方青石块要切割好作海边景石用甲方直采广东珠海', 21, '2023-10-21 17:08:08', '15976995901', '19', '广东/珠海', '女士');
INSERT INTO `resource_info` VALUES (2488, '300米长汉白玉贴坐凳，平板是6个厚立板要2个厚加安装加微信河南周口', 21, '2023-10-21 15:29:02', '18037319608', '19', '河南/周口', '先生');
INSERT INTO `resource_info` VALUES (2489, '工程采购路沿石和平铺石材加微信有采购清单山东临沂', 21, '2023-10-21 15:21:23', '15376031002', '99', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (2490, '工程采购天然石材的路沿石加微信发清单河北廊坊', 21, '2023-10-21 14:41:02', '13910821327', '99', '河北/邯郸', '先生');
INSERT INTO `resource_info` VALUES (2491, '工程采购花岗岩石材清单还没有出来加微信沟通河南许昌', 21, '2023-10-21 13:46:40', '13673818888', '99', '河南/许昌', '先生');
INSERT INTO `resource_info` VALUES (2492, '几十平1.8-2个厚的将军红平铺石材对接做装修的山东烟台', 21, '2023-10-21 13:46:06', '18266450138', '9', '山东/烟台', '先生');
INSERT INTO `resource_info` VALUES (2493, '40多平将军红2公分300X600山东济南', 21, '2023-10-21 13:38:42', '13869172712', '9', '山东/济南', '先生');
INSERT INTO `resource_info` VALUES (2494, '300平湖南小冰花石材5个厚江西九江', 21, '2023-10-21 13:37:39', '13979286691', '19', '江西/九江', '先生');
INSERT INTO `resource_info` VALUES (2495, '二次来电自己是销售墓碑的找张家界黑石材厂采购加工好的墓碑石湖南邵阳', 21, '2023-10-21 11:20:17', '13973921162', '49', '湖南/邵阳', '先生');
INSERT INTO `resource_info` VALUES (2496, '路沿石1000多米加微信客户确定尺寸发过来河南周口', 21, '2023-10-21 10:49:09', '19913356333', '109', '河南/周口', '先生');
INSERT INTO `resource_info` VALUES (2497, '二次来电2000平芝麻白600X600、300x600西藏拉萨', 21, '2023-10-21 10:01:56', '18233303000', '159', '西藏/拉萨', '先生');
INSERT INTO `resource_info` VALUES (2498, '50平将军红铺院子用山东淄博', 21, '2023-10-21 09:09:25', '18560870173', '9', '山东/淄博', '先生');
INSERT INTO `resource_info` VALUES (2499, '160米芝麻灰路沿石1.2米长x10x25福建龙岩', 21, '2023-10-20 20:43:47', '18959059036', '9', '福建/龙岩', '先生');
INSERT INTO `resource_info` VALUES (2500, '几十平黄金麻平铺石材湖南娄底', 21, '2023-10-20 20:00:12', '15697380008', '9', '湖南/娄底', '先生');
INSERT INTO `resource_info` VALUES (2501, '10几踏步的将军红石材做楼梯踏步河南周口', 21, '2023-10-20 18:49:46', '13838605596', '9', '河南/周口', '先生');
INSERT INTO `resource_info` VALUES (2502, '2200方井冈山红花岗岩江西上饶', 21, '2023-10-20 18:24:50', '18779350558', '259', '江西/上饶', '先生');
INSERT INTO `resource_info` VALUES (2503, '115米花岗岩路沿石芝麻灰350X150X600陕西咸阳', 21, '2023-10-20 17:51:34', '18292909368', '9', '陕西/咸阳', '先生');
INSERT INTO `resource_info` VALUES (2504, '500平黄金麻石材墙面用的山东聊城', 21, '2023-10-20 16:38:02', '13563596966', '39', '山东/聊城', '先生');
INSERT INTO `resource_info` VALUES (2505, '工程采购路沿石和平铺石材在开车要求加微信聊，客户指定找福建厂家浙江温州\n', 21, '2023-10-20 16:33:09', '13732001234', '99', '浙江/温州', '先生');
INSERT INTO `resource_info` VALUES (2506, '路沿石300-400米长度1米80或60都行江苏南京', 21, '2023-10-20 15:54:42', '18014452080', '29', '江苏/南京', '先生');
INSERT INTO `resource_info` VALUES (2507, '50米花坛用路沿石浙江金华', 21, '2023-10-20 15:21:23', '13966182570', '9', '浙江/金华', '先生');
INSERT INTO `resource_info` VALUES (2508, '将军红毛板客户是开石材厂的找厂家拿货安徽滁州', 21, '2023-10-20 13:45:40', '17355023859', '99', '安徽/滁州', '先生');
INSERT INTO `resource_info` VALUES (2509, '80个50公分挡车球湖南湘西', 21, '2023-10-20 12:01:24', '13574362706', '49', '湖南/湘西土家族苗族自治州', '先生');
INSERT INTO `resource_info` VALUES (2510, '工程上要采购一些路沿石和平铺的石材加微信有详单广东深圳南山区', 21, '2023-10-20 10:54:56', '13538139622', '49', '广东/深圳/南山区', '先生');
INSERT INTO `resource_info` VALUES (2511, '300立方指定湖北麻城的麻石路沿石30X10X长不限广东东莞', 21, '2023-10-20 10:38:41', '18979471675', '99', '广东/东莞', '先生');
INSERT INTO `resource_info` VALUES (2512, '500米花岗岩路沿石山东菏泽', 21, '2023-10-20 09:44:37', '15763380797', '39', '山东/荷泽', '先生');
INSERT INTO `resource_info` VALUES (2513, '三层楼共800平灰色花岗岩需要包施工山东烟台', 21, '2023-10-20 09:34:57', '13573566931', '99', '山东/烟台', '先生');
INSERT INTO `resource_info` VALUES (2514, '2000平芝麻灰600X600、300x600、150x600西藏拉萨', 21, '2023-10-20 09:05:44', '18233303000', '159', '西藏/拉萨', '先生');
INSERT INTO `resource_info` VALUES (2515, '80立方花岗岩路沿石、2000平5公分板材江西鹰潭', 21, '2023-10-20 09:04:53', '13707011201', '159', '江西/鹰潭', '先生');
INSERT INTO `resource_info` VALUES (2516, '300平芝麻灰平铺院子的石材2个厚湖北武汉', 21, '2023-10-19 21:01:29', '13507115949', '19', '湖北/武汉', '先生');
INSERT INTO `resource_info` VALUES (2517, '1500平芝麻灰石材5个厚广东惠州', 21, '2023-10-19 20:44:53', '15827796193', '159', '广东/惠州', '先生');
INSERT INTO `resource_info` VALUES (2518, '300平廉江花2.5和3个厚600X900广东茂名化州', 21, '2023-10-19 20:40:38', '14778680558', '19', '广东/茂名/化州', '先生');
INSERT INTO `resource_info` VALUES (2519, '项目采购芝麻灰路沿石和平铺石材加微信过几天发清单浙江衢州', 21, '2023-10-19 19:54:42', '15857006728', '99', '浙江/衢州', '先生');
INSERT INTO `resource_info` VALUES (2520, '230平芝麻白和芝麻黑三个厚有300X600和400X400和200X2000江西吉安', 21, '2023-10-19 19:44:55', '15170668139', '19', '江西/吉安', '先生');
INSERT INTO `resource_info` VALUES (2521, '二次来电6000平湛江黑石材3个厚300的600的和150的广东惠州', 21, '2023-10-19 18:01:11', '19925153467', '259', '广东/惠州', '先生');
INSERT INTO `resource_info` VALUES (2522, '100多平黄金麻300X600湖南永州', 21, '2023-10-19 14:32:33', '13672658878', '9', '湖南/永州', '先生');
INSERT INTO `resource_info` VALUES (2523, '60米30X10X长不限的的花岗岩路沿石颜色不限价格实惠就行山东济南', 21, '2023-10-19 14:31:38', '18364206122', '9', '山东/济南', '先生');
INSERT INTO `resource_info` VALUES (2524, '路沿石几百米尺寸10×20建议本地厂家联系福建泉州', 21, '2023-10-19 13:40:48', '17359593363', '69', '福建/泉州', '先生');
INSERT INTO `resource_info` VALUES (2525, '100平芝麻灰和芝麻白的地铺石江苏苏州市区', 21, '2023-10-19 13:37:51', '15312191236', '9', '江苏/苏州', '先生');
INSERT INTO `resource_info` VALUES (2526, '自家阳台铺的将军红石材晚一点电联山东青岛', 21, '2023-10-19 13:33:05', '15192739199', '9', '山东/青岛', '先生');
INSERT INTO `resource_info` VALUES (2527, '芝麻灰100多平想定制尺寸1.5米×50厚度30厘米公分河南南阳淅川县\n', 21, '2023-10-19 11:52:55', '13972466998', '19', '河南', '先生');
INSERT INTO `resource_info` VALUES (2528, '3000平珍珠白3公分荔枝面安徽马鞍山', 21, '2023-10-19 11:30:15', '13585229499', '159', '安徽/马鞍山', '先生');
INSERT INTO `resource_info` VALUES (2529, '工程采购红色的花岗岩石材墓碑石80x70x10个厚加微信发图片浙江嘉兴', 21, '2023-10-19 10:53:36', '13867388584', '99', '浙江/嘉兴', '先生');
INSERT INTO `resource_info` VALUES (2530, '4000米芝麻灰路沿石发短信加微信广西防城港', 21, '2023-10-19 09:50:12', '13339283853', '199', '广西/防城港', '先生');
INSERT INTO `resource_info` VALUES (5431, '咨询40个车位膜结构停车棚对接业主四川成都温江区', 3, '2023-11-09 21:05:46', '17265866372', '139', '四川/成都/温江区', '先生');
INSERT INTO `resource_info` VALUES (5432, '咨询膜结构汽车停车棚加微信沟通建议本地厂家联系福建福州', 3, '2023-11-09 21:03:27', '13600722228', '49', '福建/福州', '先生');
INSERT INTO `resource_info` VALUES (5433, '咨询公司里规划做20多个车位膜结构车棚另有两个小型钢结构房子也想一起做湖南娄底', 3, '2023-11-09 20:55:30', '18173807148', '49', '湖南/娄底', '先生');
INSERT INTO `resource_info` VALUES (5434, '2个17米长膜结构汽车车棚山西忻州', 3, '2023-11-09 20:51:35', '18335038865', '79', '山西/忻州/原平', '先生');
INSERT INTO `resource_info` VALUES (5435, '5个车位膜结车棚 7字型陕西西安 ', 3, '2023-11-09 20:39:04', '18992594668', '49', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (5436, '300平膜结构停车棚对接业主浙江绍兴', 3, '2023-11-09 20:36:35', '18888768885', '139', '浙江/绍兴/诸暨', '先生');
INSERT INTO `resource_info` VALUES (5437, '网红打卡点做膜结构遮阳棚加微信发资料看看广西贺州', 3, '2023-11-09 19:44:57', '18378481688', '99', '广西/贺州', '先生');
INSERT INTO `resource_info` VALUES (5438, '停车场的膜结构车棚有图纸加微信甘肃武威', 3, '2023-11-09 19:25:16', '19993560009', '99', '甘肃/武威', '先生');
INSERT INTO `resource_info` VALUES (5439, '7米X2米膜结构电动自行车停车棚小区做好的话后期还有河南濮阳', 3, '2023-11-09 19:21:30', '13461753030', '9', '河南/濮阳', '先生');
INSERT INTO `resource_info` VALUES (5440, '几个地方共计400平膜结构停车棚对接业主贵州黔南福泉', 3, '2023-11-10 14:15:51', '15885446888', '159', '贵州/黔南布依族苗族自治州/福泉', '先生');
INSERT INTO `resource_info` VALUES (5441, '咨询3个车位的膜结构车棚浙江宁波', 3, '2023-11-09 19:16:23', '13600612157', '19', '浙江/宁波', '先生');
INSERT INTO `resource_info` VALUES (5442, '三次来电50X5膜结构汽车车棚陕西宝鸡', 3, '2023-11-10 14:13:00', '13571705496', '139', '陕西/宝鸡', '先生');
INSERT INTO `resource_info` VALUES (5443, '15x30的电动推拉棚4米高江苏淮安', 3, '2023-11-10 14:08:59', '15189595814', '79', '江苏/淮安/淮阴区', '先生');
INSERT INTO `resource_info` VALUES (5444, '30平膜结构遮阳棚自家做安徽六安', 3, '2023-11-10 14:06:34', '18860495136', '9', '安徽/六安', '先生');
INSERT INTO `resource_info` VALUES (5445, '电动推拉棚长40米10多米宽湖北黄冈', 3, '2023-11-10 14:05:40', '13872018043', '139', '湖北/黄冈', '先生');
INSERT INTO `resource_info` VALUES (5446, '咨询15X20约六米高堆放粮食的电动推拉棚江苏淮安', 3, '2023-11-10 13:57:31', '15161763532', '29', '江苏/淮安/盱眙县', '先生');
INSERT INTO `resource_info` VALUES (5447, '找张拉膜生产厂家采购膜结构，自己平时是施工的建议省内厂家联系广东广州', 3, '2023-11-10 13:52:43', '13302253513', '99', '广东/广州', '女士');
INSERT INTO `resource_info` VALUES (5448, '79x18x6米电动推拉棚个人做浙江衢州', 3, '2023-11-10 13:40:28', '13757008315', '229', '浙江/衢州', '先生');
INSERT INTO `resource_info` VALUES (5449, '20个车位膜结构汽车棚个人做山东济南', 3, '2023-11-10 13:38:06', '15954561521', '139', '山东/济南', '先生');
INSERT INTO `resource_info` VALUES (5450, '二次留言 800平膜结构停车棚对接业主广东广州', 3, '2023-11-10 13:33:48', '13826173017', '199', '广东/广州', '先生');
INSERT INTO `resource_info` VALUES (5451, '8个车位膜结构汽车停车棚对接业主山西长治', 3, '2023-11-09 19:13:40', '15510155261', '79', '山西/长治', '先生');
INSERT INTO `resource_info` VALUES (5452, '50X8膜结构停车棚对接业主河北石家庄', 3, '2023-11-09 18:58:01', '13832327698', '159', '河北/石家庄', '先生');
INSERT INTO `resource_info` VALUES (5453, '10米X3的膜结构车棚停湖北-黄冈', 3, '2023-11-09 18:55:32', '15327716069', '19', '湖北/黄冈', '先生');
INSERT INTO `resource_info` VALUES (5454, '停放10辆电瓶车膜结构停车棚安徽宿州', 3, '2023-11-09 18:11:41', '13956855172', '19', '安徽/宿州', '先生');
INSERT INTO `resource_info` VALUES (5455, '咨询厂区里规划做1000平方膜结构停车棚四川成都', 3, '2023-11-09 17:45:19', '13408066335', '159', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5456, '6x16x3.8米电动或手动的推拉棚山东滨州', 3, '2023-11-09 17:40:53', '13589740999', '29', '山东/滨州', '先生');
INSERT INTO `resource_info` VALUES (5457, '20X30X2.5米电动推拉棚陕西铜川', 3, '2023-11-09 17:19:52', '13571403326', '159', '陕西/铜川', '先生');
INSERT INTO `resource_info` VALUES (5458, '客户是做门窗的经常接到膜结构的项目都是交给别人做，近期有客户询问污水池加盖的项目想找厂家合作江苏连云港', 3, '2023-11-09 17:16:44', '15855389408', '99', '江苏/连云港', '先生');
INSERT INTO `resource_info` VALUES (5459, '50多平膜结构汽车车棚 四川泸州', 3, '2023-11-10 13:28:17', '18111177828', '29', '四川/泸州', '女士');
INSERT INTO `resource_info` VALUES (5460, '小区膜结构电动自行车棚对接业主委员会山东烟台蓬莱', 3, '2023-11-09 17:14:02', '13805451762', '49', '山东/烟台/蓬莱', '先生');
INSERT INTO `resource_info` VALUES (5461, '空地上做12X12电动推拉棚山西运城', 3, '2023-11-10 12:00:30', '18903595246', '49', '山西/运城', '女士');
INSERT INTO `resource_info` VALUES (5462, '膜结构景观棚100多平自家院子做湖南长沙', 3, '2023-11-09 17:11:15', '15874189488', '59', '湖南/长沙', '先生');
INSERT INTO `resource_info` VALUES (5463, '咨询 客户是做充电桩的有项目要做找膜结构厂家合作目前在设计阶段加微信发资料福建', 3, '2023-11-10 11:35:20', '13696967755', '49', '福建', '先生');
INSERT INTO `resource_info` VALUES (5464, '咨询膜结构停车棚要求加微信聊北京北京\n', 3, '2023-11-10 11:24:18', '17718369917', '49', '北京/北京', '女士');
INSERT INTO `resource_info` VALUES (5465, '4X6的膜结构车棚湖北荆州 ', 3, '2023-11-10 11:18:57', '18608613966', '19', '湖北/荆州', '先生');
INSERT INTO `resource_info` VALUES (5466, '膜结构停车棚70米长2-3米宽要求钢结构和张拉膜分开报价广东江门', 3, '2023-11-10 11:14:08', '13555650421', '69', '广东/江门', '先生');
INSERT INTO `resource_info` VALUES (5467, '10X10手动推拉棚滑冰场换鞋通道做需要密封好一点的加微信发案例图片山西吕梁', 3, '2023-11-10 11:09:15', '15035359295', '29', '山西/吕梁', '先生');
INSERT INTO `resource_info` VALUES (5468, '100多平膜结构停车棚加微信四川成都', 3, '2023-11-10 10:54:48', '18608008804', '49', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5469, '电动推拉棚15×10高5米江苏徐州', 3, '2023-11-10 10:44:27', '13375114189', '39', '江苏/徐州', '先生');
INSERT INTO `resource_info` VALUES (5470, '300平膜结构停车棚个人做新疆哈密', 3, '2023-11-10 10:17:50', '19190499891', '139', '新疆/哈密地区', '先生');
INSERT INTO `resource_info` VALUES (5471, '膜结构自行车停车棚18×6江苏南京', 3, '2023-11-09 17:07:15', '13851433195', '49', '江苏/南京/江宁区', '先生');
INSERT INTO `resource_info` VALUES (5472, '膜结构汽车停车棚20米长安徽芜湖', 3, '2023-11-09 17:02:37', '17730330800', '49', '安徽/芜湖', '先生');
INSERT INTO `resource_info` VALUES (5473, '咨询5个车位膜结构车棚一排的一边可靠墙河北邯郸', 3, '2023-11-09 16:39:17', '15931012666', '29', '河北/邯郸', '先生');
INSERT INTO `resource_info` VALUES (5474, '咨询1000平的电动推拉棚4米高河北-衡水', 3, '2023-11-09 16:29:33', '17531809920', '139', '河北/衡水', '先生');
INSERT INTO `resource_info` VALUES (5475, '电动推拉棚1000平江苏盐城', 3, '2023-11-09 16:29:05', '18852468493', '199', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (5476, '6X10米电动推拉棚做在墙上陕西渭南', 3, '2023-11-09 16:28:04', '18142320092', '29', '陕西/渭南', '先生');
INSERT INTO `resource_info` VALUES (5477, '20X20X4米电动推拉棚山西长治', 3, '2023-11-09 16:05:51', '18903551857', '139', '山西/长治', '先生');
INSERT INTO `resource_info` VALUES (5478, '咨询膜结构停车棚加微信发资料建议本地厂家联系天津天津', 3, '2023-11-10 10:14:15', '13920074334', '49', '天津/天津', '先生');
INSERT INTO `resource_info` VALUES (5479, '小区膜结构7字型电动自行车棚河北廊坊', 3, '2023-11-09 16:02:59', '13932698269', '99', '河北/廊坊', '先生');
INSERT INTO `resource_info` VALUES (5480, '4个车位膜结构停车棚对接业主湖北黄石', 3, '2023-11-10 10:10:52', '13995972823', '39', '湖北/黄石', '先生');
INSERT INTO `resource_info` VALUES (5481, '中标了5亩地大小的空地给规划一下做电动推拉棚建议本地厂家联系河北秦皇岛卢龙县', 3, '2023-11-09 15:40:57', '17744509450', '299', '河北/秦皇岛/卢龙县', '先生');
INSERT INTO `resource_info` VALUES (5482, '20个车位膜结构汽车棚工厂做山东烟台龙口', 3, '2023-11-10 10:00:34', '15564502222', '139', '山东/烟台/龙口', '先生');
INSERT INTO `resource_info` VALUES (5483, '7x90米长电动推拉棚，只做顶部借助之前的立柱因之前彩钢是违建拆除了，可到现场面谈重庆长寿', 3, '2023-11-09 15:25:26', '15923337398', '79', '重庆/长寿区/重庆', '先生');
INSERT INTO `resource_info` VALUES (5484, '600平膜结构遮阳棚儿童乐园做建议本地厂家联系山东临沂', 3, '2023-11-10 09:54:45', '18305316969', '199', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (5485, '膜结构汽车停车棚100平在小区做湖南常德\n', 3, '2023-11-10 09:53:10', '13647361927', '49', '湖南/常德', '先生');
INSERT INTO `resource_info` VALUES (5486, '机关单位做膜结构停车棚加微信客户发尺寸需要出方案建议本地厂家联系北京-北京', 3, '2023-11-10 09:52:49', '13911258369', '99', '北京/密云县/北京', '先生');
INSERT INTO `resource_info` VALUES (5487, '95X7米T字型（一侧2米一侧5米）需要设计及设计院盖章单位里做安徽合肥', 3, '2023-11-10 09:49:53', '17344004594', '199', '安徽/合肥', '先生');
INSERT INTO `resource_info` VALUES (5488, '小区膜结构自行车棚对接物业负责人安徽蚌埠', 3, '2023-11-10 09:44:02', '15805528498', '99', '安徽/蚌埠', '先生');
INSERT INTO `resource_info` VALUES (5489, '咨询膜结构停车棚想在景区做加微信聊甘肃陇南', 3, '2023-11-10 09:38:53', '18993092288', '49', '甘肃/陇南', '先生');
INSERT INTO `resource_info` VALUES (5490, '30多平膜结构电动自行车棚，加微信发图纸河南许昌', 3, '2023-11-10 09:11:38', '13603951288', '29', '河南/许昌', '先生');
INSERT INTO `resource_info` VALUES (5491, '60平膜结构停车棚自家做福建泉州', 3, '2023-11-09 15:21:32', '13015860688', '39', '福建/泉州', '先生');
INSERT INTO `resource_info` VALUES (5492, '咨询膜结构停车棚400平个人投建贵州贵阳', 3, '2023-11-09 15:20:41', '13985159585', '79', '贵州/贵阳', '先生');
INSERT INTO `resource_info` VALUES (5493, '咨询膜结构自行车棚100平山东临沂', 3, '2023-11-09 15:10:02', '15553981058', '19', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (5494, '20平遮阳棚海南陵水', 3, '2023-11-09 15:09:47', '18331608035', '19', '海南', '先生');
INSERT INTO `resource_info` VALUES (5495, '39X10X3.5米高电动推拉棚养殖场做湖南怀化溆浦县', 3, '2023-11-09 14:57:28', '13814103082', '139', '湖南/怀化/溆浦县', '女士');
INSERT INTO `resource_info` VALUES (5496, '两栋房子中间做12X12电动推拉棚江苏南通', 3, '2023-11-09 14:46:29', '18962854808', '49', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5497, '三次留言 80平膜结构电动自行车停车棚对接业主河北廊坊', 3, '2023-11-09 14:32:09', '15931463957', '49', '河北/廊坊', '先生');
INSERT INTO `resource_info` VALUES (5498, '10x28的膜结构棚高8米浙江台州', 3, '2023-11-09 14:24:11', '13291663833', '139', '浙江/台州', '先生');
INSERT INTO `resource_info` VALUES (5499, '180平膜结构停车棚与承接人联系广东深圳', 3, '2023-11-10 09:05:26', '13823295186', '79', '广东/深圳', '先生');
INSERT INTO `resource_info` VALUES (5500, '需要建电动推拉棚加微信明天回去测量江西吉安', 3, '2023-11-09 14:16:23', '13907962897', '49', '江西/吉安', '先生');
INSERT INTO `resource_info` VALUES (5501, '18平膜结构雨棚江苏扬州广陵', 3, '2023-11-10 09:03:20', '13705273855', '19', '江苏/扬州/广陵区', '先生');
INSERT INTO `resource_info` VALUES (5502, '咨询20X50的电动推拉棚湖北武汉黄陂区', 3, '2023-11-09 14:14:28', '13487097960', '139', '湖北/武汉/黄陂区', '先生');
INSERT INTO `resource_info` VALUES (5503, '5-6个车场每个车场20-40个车位，膜结构汽车棚个人投建福建福州', 3, '2023-11-10 09:01:06', '15259175888', '399', '福建/福州', '先生');
INSERT INTO `resource_info` VALUES (5504, '80-100个车位膜结构汽车棚个人投建加微信新疆乌鲁木齐', 3, '2023-11-10 08:57:13', '18167974444', '259', '新疆/乌鲁木齐', '先生');
INSERT INTO `resource_info` VALUES (5505, '钢结构彩钢车棚几十平可约时间去测量尺寸四川遂宁', 3, '2023-11-10 08:53:48', '13778718967', '19', '四川/遂宁', '先生');
INSERT INTO `resource_info` VALUES (5506, '膜结构停车棚要求技术人员加微信发图纸先电话联系贵州贵阳', 3, '2023-11-10 08:45:34', '18198691068', '99', '贵州/贵阳', '先生');
INSERT INTO `resource_info` VALUES (5507, '膜结构停车棚50平家用江苏泰州', 3, '2023-11-10 08:45:06', '13218720966', '29', '江苏/泰州/泰兴', '先生');
INSERT INTO `resource_info` VALUES (5508, '两到三个车位膜结构汽车棚客户目前人在四川成都', 3, '2023-11-10 08:24:46', '13691703180', '29', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5509, '单位做两个钢结构玻璃钢雨棚建议本地厂家联系北京北京', 3, '2023-11-10 08:22:33', '13701170343', '49', '北京/北京', '先生');
INSERT INTO `resource_info` VALUES (5510, '300平膜结构充电桩棚，加微信与承接人联系湖北武汉', 3, '2023-11-10 08:21:34', '15071357272', '139', '湖北/武汉', '先生');
INSERT INTO `resource_info` VALUES (5511, '咨询电动推拉棚12×10河南平顶山', 3, '2023-11-09 13:59:53', '13087066139', '29', '河南/平顶山', '先生');
INSERT INTO `resource_info` VALUES (5512, '70-80平膜结构景观棚户外会所做对接业主四川成都', 3, '2023-11-09 13:58:09', '13688309088', '49', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5513, '电动推拉棚10×50高3米多河南商丘', 3, '2023-11-09 13:53:13', '15993963298', '139', '河南/商丘', '先生');
INSERT INTO `resource_info` VALUES (5514, '11x12x3米手动推拉棚江苏南通', 3, '2023-11-09 13:46:24', '13222127776', '49', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5515, '80平膜结构遮阳棚自家做江苏常州', 3, '2023-11-09 13:33:57', '15851912829', '49', '江苏/常州', '先生');
INSERT INTO `resource_info` VALUES (5516, '需要建个20米长的电动推拉棚湖南常德', 3, '2023-11-09 13:32:26', '13975692660', '29', '湖南', '先生');
INSERT INTO `resource_info` VALUES (5517, '咨询40个车位膜结构停车棚对接业主四川成都温江区', 3, '2023-11-09 21:05:46', '17265866372', '139', '四川/成都/温江区', '先生');
INSERT INTO `resource_info` VALUES (5518, '咨询膜结构汽车停车棚加微信沟通建议本地厂家联系福建福州', 3, '2023-11-09 21:03:27', '13600722228', '49', '福建/福州', '先生');
INSERT INTO `resource_info` VALUES (5519, '空地上做20米长30米宽电动推拉棚广东江门', 3, '2023-11-09 13:30:19', '13631848948', '159', '广东/江门', '先生');
INSERT INTO `resource_info` VALUES (5520, '咨询公司里规划做20多个车位膜结构车棚另有两个小型钢结构房子也想一起做湖南娄底', 3, '2023-11-09 20:55:30', '18173807148', '49', '湖南/娄底', '先生');
INSERT INTO `resource_info` VALUES (5521, '10个大巴车车位膜结构停车棚客运站做的不需要投标云南曲靖', 3, '2023-11-09 12:00:10', '13170703666', '79', '云南/曲靖', '先生');
INSERT INTO `resource_info` VALUES (5522, '2个17米长膜结构汽车车棚山西忻州', 3, '2023-11-09 20:51:35', '18335038865', '79', '山西/忻州/原平', '先生');
INSERT INTO `resource_info` VALUES (5523, '300平工厂膜结构汽车棚，客户要求到现场实地测量设计广东茂名', 3, '2023-11-09 11:52:09', '13902546161', '139', '广东/茂名', '先生');
INSERT INTO `resource_info` VALUES (5524, '5个车位膜结车棚 7字型陕西西安 ', 3, '2023-11-09 20:39:04', '18992594668', '49', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (5525, '电动推拉棚20×40高6米江苏南通', 3, '2023-11-09 11:46:43', '17305257933', '159', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5526, '300平膜结构停车棚对接业主浙江绍兴', 3, '2023-11-09 20:36:35', '18888768885', '139', '浙江/绍兴/诸暨', '先生');
INSERT INTO `resource_info` VALUES (5527, '网红打卡点做膜结构遮阳棚加微信发资料看看广西贺州', 3, '2023-11-09 19:44:57', '18378481688', '99', '广西/贺州', '先生');
INSERT INTO `resource_info` VALUES (5528, '停车场的膜结构车棚有图纸加微信甘肃武威', 3, '2023-11-09 19:25:16', '19993560009', '99', '甘肃/武威', '先生');
INSERT INTO `resource_info` VALUES (5529, '7米X2米膜结构电动自行车停车棚小区做好的话后期还有河南濮阳', 3, '2023-11-09 19:21:30', '13461753030', '9', '河南/濮阳', '先生');
INSERT INTO `resource_info` VALUES (5530, '咨询3个车位的膜结构车棚浙江宁波', 3, '2023-11-09 19:16:23', '13600612157', '19', '浙江/宁波', '先生');
INSERT INTO `resource_info` VALUES (5531, '咨询电动推拉棚20×50高4米多山西晋中', 3, '2023-11-09 11:41:42', '13935411717', '159', '山西/晋中', '先生');
INSERT INTO `resource_info` VALUES (5532, '小区做膜结构电动自行车棚与物业联系山东临沂', 3, '2023-11-09 11:13:24', '13583968976', '99', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (5533, '60平膜结构停车棚自家做的福建三明', 3, '2023-11-09 11:11:40', '18859889769', '39', '福建/三明', '先生');
INSERT INTO `resource_info` VALUES (5534, '100平电动推拉遮阳棚个人做江苏扬州宝应', 3, '2023-11-09 11:07:30', '18012323929', '29', '江苏/扬州/宝应县', '女士');
INSERT INTO `resource_info` VALUES (5535, '8个车位膜结构汽车停车棚对接业主山西长治', 3, '2023-11-09 19:13:40', '15510155261', '79', '山西/长治', '先生');
INSERT INTO `resource_info` VALUES (5536, '咨询200平的电动推拉棚约5米高厂区空地上做江苏盐城', 3, '2023-11-09 11:01:53', '18921847717', '29', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (5537, '50X8膜结构停车棚对接业主河北石家庄', 3, '2023-11-09 18:58:01', '13832327698', '159', '河北/石家庄', '先生');
INSERT INTO `resource_info` VALUES (5538, '15X6X4.5米电动或手动推拉棚分别报价个人做福建龙岩', 3, '2023-11-09 11:00:21', '13859542748', '49', '福建/龙岩', '先生');
INSERT INTO `resource_info` VALUES (5539, '10米X3的膜结构车棚停湖北-黄冈', 3, '2023-11-09 18:55:32', '15327716069', '19', '湖北/黄冈', '先生');
INSERT INTO `resource_info` VALUES (5540, '14x28的电动推拉棚厂区做，只需要做上面棚做1.5高陕西西安', 3, '2023-11-09 10:52:30', '13659144812', '49', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (5541, '停放10辆电瓶车膜结构停车棚安徽宿州', 3, '2023-11-09 18:11:41', '13956855172', '19', '安徽/宿州', '先生');
INSERT INTO `resource_info` VALUES (5542, '20多个车位膜结构汽车棚与承接人联系山西运城', 3, '2023-11-09 10:52:30', '13934865332', '139', '山西', '先生');
INSERT INTO `resource_info` VALUES (5543, '咨询厂区里规划做1000平方膜结构停车棚四川成都', 3, '2023-11-09 17:45:19', '13408066335', '159', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5544, '1000平4.5米高的膜结构棚固定式的做仓库用安徽亳州', 3, '2023-11-09 10:44:19', '13399671988', '229', '安徽/亳州', '先生');
INSERT INTO `resource_info` VALUES (5545, '6x16x3.8米电动或手动的推拉棚山东滨州', 3, '2023-11-09 17:40:53', '13589740999', '29', '山东/滨州', '先生');
INSERT INTO `resource_info` VALUES (5546, '120平（40X3）的膜结构车棚河南开封', 3, '2023-11-09 10:30:14', '13837831232', '79', '河南/开封', '先生');
INSERT INTO `resource_info` VALUES (5547, '20X30X2.5米电动推拉棚陕西铜川', 3, '2023-11-09 17:19:52', '13571403326', '159', '陕西/铜川', '先生');
INSERT INTO `resource_info` VALUES (5548, '客户是做门窗的经常接到膜结构的项目都是交给别人做，近期有客户询问污水池加盖的项目想找厂家合作江苏连云港', 3, '2023-11-09 17:16:44', '15855389408', '99', '江苏/连云港', '先生');
INSERT INTO `resource_info` VALUES (5549, '小区膜结构电动自行车棚对接业主委员会山东烟台蓬莱', 3, '2023-11-09 17:14:02', '13805451762', '49', '山东/烟台/蓬莱', '先生');
INSERT INTO `resource_info` VALUES (5550, '膜结构景观棚100多平自家院子做湖南长沙', 3, '2023-11-09 17:11:15', '15874189488', '59', '湖南/长沙', '先生');
INSERT INTO `resource_info` VALUES (5551, '膜结构自行车停车棚18×6江苏南京', 3, '2023-11-09 17:07:15', '13851433195', '49', '江苏/南京/江宁区', '先生');
INSERT INTO `resource_info` VALUES (5552, '膜结构汽车停车棚20米长安徽芜湖', 3, '2023-11-09 17:02:37', '17730330800', '49', '安徽/芜湖', '先生');
INSERT INTO `resource_info` VALUES (5553, '咨询5个车位膜结构车棚一排的一边可靠墙河北邯郸', 3, '2023-11-09 16:39:17', '15931012666', '29', '河北/邯郸', '先生');
INSERT INTO `resource_info` VALUES (5554, '咨询1000平的电动推拉棚4米高河北-衡水', 3, '2023-11-09 16:29:33', '17531809920', '139', '河北/衡水', '先生');
INSERT INTO `resource_info` VALUES (5555, '电动推拉棚1000平江苏盐城', 3, '2023-11-09 16:29:05', '18852468493', '199', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (5556, '6X10米电动推拉棚做在墙上陕西渭南', 3, '2023-11-09 16:28:04', '18142320092', '29', '陕西/渭南', '先生');
INSERT INTO `resource_info` VALUES (5557, '20X20X4米电动推拉棚山西长治', 3, '2023-11-09 16:05:51', '18903551857', '139', '山西/长治', '先生');
INSERT INTO `resource_info` VALUES (5558, '小区膜结构7字型电动自行车棚河北廊坊', 3, '2023-11-09 16:02:59', '13932698269', '99', '河北/廊坊', '先生');
INSERT INTO `resource_info` VALUES (5559, '中标了5亩地大小的空地给规划一下做电动推拉棚建议本地厂家联系河北秦皇岛卢龙县', 3, '2023-11-09 15:40:57', '17744509450', '299', '河北/秦皇岛/卢龙县', '先生');
INSERT INTO `resource_info` VALUES (5560, '7x90米长电动推拉棚，只做顶部借助之前的立柱因之前彩钢是违建拆除了，可到现场面谈重庆长寿', 3, '2023-11-09 15:25:26', '15923337398', '79', '重庆/长寿区/重庆', '先生');
INSERT INTO `resource_info` VALUES (5561, '60平膜结构停车棚自家做福建泉州', 3, '2023-11-09 15:21:32', '13015860688', '39', '福建/泉州', '先生');
INSERT INTO `resource_info` VALUES (5562, '咨询膜结构停车棚400平个人投建贵州贵阳', 3, '2023-11-09 15:20:41', '13985159585', '79', '贵州/贵阳', '先生');
INSERT INTO `resource_info` VALUES (5563, '咨询膜结构自行车棚100平山东临沂', 3, '2023-11-09 15:10:02', '15553981058', '19', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (5564, '20平遮阳棚海南陵水', 3, '2023-11-09 15:09:47', '18331608035', '19', '海南', '先生');
INSERT INTO `resource_info` VALUES (5565, '39X10X3.5米高电动推拉棚养殖场做湖南怀化溆浦县', 3, '2023-11-09 14:57:28', '13814103082', '139', '湖南/怀化/溆浦县', '女士');
INSERT INTO `resource_info` VALUES (5566, '两栋房子中间做12X12电动推拉棚江苏南通', 3, '2023-11-09 14:46:29', '18962854808', '49', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5567, '三次留言 80平膜结构电动自行车停车棚对接业主河北廊坊', 3, '2023-11-09 14:32:09', '15931463957', '49', '河北/廊坊', '先生');
INSERT INTO `resource_info` VALUES (5568, '10x28的膜结构棚高8米浙江台州', 3, '2023-11-09 14:24:11', '13291663833', '139', '浙江/台州', '先生');
INSERT INTO `resource_info` VALUES (5569, '需要建电动推拉棚加微信明天回去测量江西吉安', 3, '2023-11-09 14:16:23', '13907962897', '49', '江西/吉安', '先生');
INSERT INTO `resource_info` VALUES (5570, '咨询20X50的电动推拉棚湖北武汉黄陂区', 3, '2023-11-09 14:14:28', '13487097960', '139', '湖北/武汉/黄陂区', '先生');
INSERT INTO `resource_info` VALUES (5571, '咨询电动推拉棚12×10河南平顶山', 3, '2023-11-09 13:59:53', '13087066139', '29', '河南/平顶山', '先生');
INSERT INTO `resource_info` VALUES (5572, '70-80平膜结构景观棚户外会所做对接业主四川成都', 3, '2023-11-09 13:58:09', '13688309088', '49', '四川/成都', '先生');
INSERT INTO `resource_info` VALUES (5573, '电动推拉棚10×50高3米多河南商丘', 3, '2023-11-09 13:53:13', '15993963298', '139', '河南/商丘', '先生');
INSERT INTO `resource_info` VALUES (5574, '11x12x3米手动推拉棚江苏南通', 3, '2023-11-09 13:46:24', '13222127776', '49', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5575, '80平膜结构遮阳棚自家做江苏常州', 3, '2023-11-09 13:33:57', '15851912829', '49', '江苏/常州', '先生');
INSERT INTO `resource_info` VALUES (5576, '需要建个20米长的电动推拉棚湖南常德', 3, '2023-11-09 13:32:26', '13975692660', '29', '湖南', '先生');
INSERT INTO `resource_info` VALUES (5577, '空地上做20米长30米宽电动推拉棚广东江门', 3, '2023-11-09 13:30:19', '13631848948', '159', '广东/江门', '先生');
INSERT INTO `resource_info` VALUES (5578, '10个大巴车车位膜结构停车棚客运站做的不需要投标云南曲靖', 3, '2023-11-09 12:00:10', '13170703666', '79', '云南/曲靖', '先生');
INSERT INTO `resource_info` VALUES (5579, '300平工厂膜结构汽车棚，客户要求到现场实地测量设计广东茂名', 3, '2023-11-09 11:52:09', '13902546161', '139', '广东/茂名', '先生');
INSERT INTO `resource_info` VALUES (5580, '电动推拉棚20×40高6米江苏南通', 3, '2023-11-09 11:46:43', '17305257933', '159', '江苏/南通', '先生');
INSERT INTO `resource_info` VALUES (5581, '咨询电动推拉棚20×50高4米多山西晋中', 3, '2023-11-09 11:41:42', '13935411717', '159', '山西/晋中', '先生');
INSERT INTO `resource_info` VALUES (5582, '小区做膜结构电动自行车棚与物业联系山东临沂', 3, '2023-11-09 11:13:24', '13583968976', '99', '山东/临沂', '先生');
INSERT INTO `resource_info` VALUES (5583, '60平膜结构停车棚自家做的福建三明', 3, '2023-11-09 11:11:40', '18859889769', '39', '福建/三明', '先生');
INSERT INTO `resource_info` VALUES (5584, '100平电动推拉遮阳棚个人做江苏扬州宝应', 3, '2023-11-09 11:07:30', '18012323929', '29', '江苏/扬州/宝应县', '女士');
INSERT INTO `resource_info` VALUES (5585, '咨询200平的电动推拉棚约5米高厂区空地上做江苏盐城', 3, '2023-11-09 11:01:53', '18921847717', '29', '江苏/盐城', '先生');
INSERT INTO `resource_info` VALUES (5586, '15X6X4.5米电动或手动推拉棚分别报价个人做福建龙岩', 3, '2023-11-09 11:00:21', '13859542748', '49', '福建/龙岩', '先生');
INSERT INTO `resource_info` VALUES (5587, '14x28的电动推拉棚厂区做，只需要做上面棚做1.5高陕西西安', 3, '2023-11-09 10:52:30', '13659144812', '49', '陕西/西安', '先生');
INSERT INTO `resource_info` VALUES (5588, '20多个车位膜结构汽车棚与承接人联系山西运城', 3, '2023-11-09 10:52:30', '13934865332', '139', '山西', '先生');
INSERT INTO `resource_info` VALUES (5589, '1000平4.5米高的膜结构棚固定式的做仓库用安徽亳州', 3, '2023-11-09 10:44:19', '13399671988', '229', '安徽/亳州', '先生');
INSERT INTO `resource_info` VALUES (5590, '120平（40X3）的膜结构车棚河南开封', 3, '2023-11-09 10:30:14', '13837831232', '79', '河南/开封', '先生');

-- ----------------------------
-- Table structure for star_cource
-- ----------------------------
DROP TABLE IF EXISTS `star_cource`;
CREATE TABLE `star_cource`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '教程名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '更多内容介绍',
  `ename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ename',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '教程图片',
  `category_id` int NULL DEFAULT NULL COMMENT '分类id',
  `visit_num` int NULL DEFAULT NULL COMMENT '浏览数量',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 252 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '教程表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of star_cource
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接口地址',
  `time` int NOT NULL COMMENT '耗时',
  `ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'IP',
  `param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求参数',
  `err` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '异常',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1143585680111173633 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1143538952842510336, NULL, '/login', 1251, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', '### Error querying database.  Cause: java.sql.SQLSyntaxErrorException: Table \'mars.sys_user\' doesn\'t exist', '2023-08-22 13:35:32', '2023-08-22 13:35:32', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539374084849664, 1000000000000000001, '/login', 592, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 13:37:13', '2023-08-22 13:37:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539375729016832, NULL, '/common/file/download', 148, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:37:13', '2023-08-22 13:37:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539391315050496, 1000000000000000001, '/sys/user/list', 416, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:37:17', '2023-08-22 13:37:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539395110895616, 1000000000000000001, '/sys/role/list', 280, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 13:37:18', '2023-08-22 13:37:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539400253112320, 1000000000000000001, '/sys/menu/list', 224, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 13:37:19', '2023-08-22 13:37:19', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539439025258496, 1000000000000000001, '/sys/user/list', 357, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:37:28', '2023-08-22 13:37:28', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539440996581376, 1000000000000000001, '/sys/user/list', 314, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:37:29', '2023-08-22 13:37:29', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539447615193088, 1000000000000000001, '/sys/user/list', 362, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:37:30', '2023-08-22 13:37:30', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539463113146368, 1000000000000000001, '/sys/user/list', 5366, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:37:34', '2023-08-22 13:37:34', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539553819164672, 1000000000000000001, '/tool/api/list', 48, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692682675556,\"status\":404,\"error\":\"Not Found\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 13:37:56', '2023-08-22 13:37:56', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539574241230848, 1000000000000000001, '/tool/table/list', 582, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 13:38:00', '2023-08-22 13:38:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539600367550464, 1000000000000000001, '/tool/api/list', 11, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692682686663,\"status\":404,\"error\":\"Not Found\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 13:38:07', '2023-08-22 13:38:07', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539602515034112, 1000000000000000001, '/tool/api/list', 6, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692682687177,\"status\":404,\"error\":\"Not Found\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 13:38:07', '2023-08-22 13:38:07', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143539603441975296, 1000000000000000001, '/tool/api/list', 7, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692682687397,\"status\":404,\"error\":\"Not Found\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 13:38:07', '2023-08-22 13:38:07', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540280155176960, NULL, '/common/file/download', 1096, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:40:49', '2023-08-22 13:40:49', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540288745111552, 1000000000000000001, '/tool/api/list', 66, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692682850760,\"status\":404,\"error\":\"Not Found\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 13:40:51', '2023-08-22 13:40:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540430462255104, 1000000000000000001, '/tool/table/list', 615, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 13:41:25', '2023-08-22 13:41:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540440419532800, 1000000000000000001, '/sys/user/list', 428, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 13:41:27', '2023-08-22 13:41:27', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540444488007680, 1000000000000000001, '/sys/role/list', 264, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 13:41:28', '2023-08-22 13:41:28', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540447671484416, 1000000000000000001, '/sys/menu/list', 228, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 13:41:29', '2023-08-22 13:41:29', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540490860232704, 1000000000000000001, '/sys/menu/list', 217, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 13:41:39', '2023-08-22 13:41:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540491850088448, 1000000000000000001, '/sys/menu/list', 249, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 13:41:39', '2023-08-22 13:41:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540492454068224, 1000000000000000001, '/sys/menu/list', 169, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 13:41:39', '2023-08-22 13:41:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540507020886016, NULL, '/common/file/download', 171, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:41:43', '2023-08-22 13:41:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540741247598592, 1000000000000000001, '/log/api/list', 266, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 13:42:39', '2023-08-22 13:42:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540764408545280, 1000000000000000001, '/log/api/list', 257, '127.0.0.1', '{\"pageNo\":2,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 13:42:44', '2023-08-22 13:42:44', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540768946782208, 1000000000000000001, '/log/api/list', 256, '127.0.0.1', '{\"pageNo\":3,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 13:42:45', '2023-08-22 13:42:45', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540775603142656, 1000000000000000001, '/log/api/list', 299, '127.0.0.1', '{\"pageNo\":2,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 13:42:47', '2023-08-22 13:42:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143540787481411584, 1000000000000000001, '/log/api/list', 269, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 13:42:50', '2023-08-22 13:42:50', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541241653231616, 1000000000000000001, '/logout', 2, '127.0.0.1', NULL, NULL, '2023-08-22 13:44:38', '2023-08-22 13:44:38', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541250880700416, 1000000000000000001, '/login', 510, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 13:44:40', '2023-08-22 13:44:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541252147380224, NULL, '/common/file/download', 124, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:44:40', '2023-08-22 13:44:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541272305205248, 1000000000000000001, '/logout', 1, '127.0.0.1', NULL, NULL, '2023-08-22 13:44:45', '2023-08-22 13:44:45', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541294786674688, 1000000000000000001, '/login', 489, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 13:44:51', '2023-08-22 13:44:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541296095297536, NULL, '/common/file/download', 136, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:44:51', '2023-08-22 13:44:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143541310838276096, 1000000000000000001, '/logout', 0, '127.0.0.1', NULL, NULL, '2023-08-22 13:44:54', '2023-08-22 13:44:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143544126961090560, 1000000000000000001, '/login', 1660, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 13:56:06', '2023-08-22 13:56:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143544128609452032, NULL, '/common/file/download', 157, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 13:56:06', '2023-08-22 13:56:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143544147706118144, 1000000000000000001, '/logout', 1, '127.0.0.1', NULL, NULL, '2023-08-22 13:56:11', '2023-08-22 13:56:11', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545574214074368, 1000000000000000001, '/login', 867, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 14:01:51', '2023-08-22 14:01:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545575778549760, NULL, '/common/file/download', 128, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:01:51', '2023-08-22 14:01:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545776434053120, NULL, '/common/file/download', 169, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:02:39', '2023-08-22 14:02:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545786647183360, NULL, '/common/file/download', 171, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:02:42', '2023-08-22 14:02:42', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545854083203072, 1000000000000000001, '/tool/table/list', 624, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:02:58', '2023-08-22 14:02:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143545855664455680, 1000000000000000001, '/tool/api/list', 48, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692684178038,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:02:58', '2023-08-22 14:02:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547196134653952, NULL, '/common/file/download', 629, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:08:18', '2023-08-22 14:08:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547222462300160, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692684503914,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:08:24', '2023-08-22 14:08:24', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547237662457856, 1000000000000000001, '/tool/table/list', 655, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:08:28', '2023-08-22 14:08:28', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547253617590272, 1000000000000000001, '/sys/user/list', 495, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:08:31', '2023-08-22 14:08:31', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547255945428992, 1000000000000000001, '/sys/role/list', 266, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 14:08:32', '2023-08-22 14:08:32', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547258482982912, 1000000000000000001, '/sys/menu/list', 217, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 14:08:33', '2023-08-22 14:08:33', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547533457358848, 1000000000000000001, '/tool/api/list', 8, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692684578061,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:09:38', '2023-08-22 14:09:38', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547540054999040, 1000000000000000001, '/tool/table/list', 695, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:09:40', '2023-08-22 14:09:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547542718382080, NULL, '/common/file/download', 5159, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:09:40', '2023-08-22 14:09:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547709169336320, NULL, '/common/file/download', 176, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:10:20', '2023-08-22 14:10:20', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547718463913984, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692684622170,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:10:22', '2023-08-22 14:10:22', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547724382076928, 1000000000000000001, '/tool/table/list', 607, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:10:24', '2023-08-22 14:10:24', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547931324841984, NULL, '/common/file/download', 178, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:11:13', '2023-08-22 14:11:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547947565187072, 1000000000000000001, '/sys/user/list', 351, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:11:17', '2023-08-22 14:11:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547950568308736, 1000000000000000001, '/sys/role/list', 234, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 14:11:18', '2023-08-22 14:11:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143547953152000000, 1000000000000000001, '/sys/menu/list', 178, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 14:11:18', '2023-08-22 14:11:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143548379683356672, NULL, '/common/file/download', 179, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:13:00', '2023-08-22 14:13:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143548393168044032, 1000000000000000001, '/tool/table/list', 563, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:13:03', '2023-08-22 14:13:03', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143548394459889664, 1000000000000000001, '/tool/api/list', 8, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692684783340,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:13:03', '2023-08-22 14:13:03', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143555390655430656, NULL, '/common/file/download', 1115, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:40:51', '2023-08-22 14:40:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143555640149409792, NULL, '/common/file/download', 202, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:41:51', '2023-08-22 14:41:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143555772332900352, 1000000000000000001, '/log/api/list', 356, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 14:42:22', '2023-08-22 14:42:22', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143555787449171968, 1000000000000000001, '/tool/api/list', 44, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692686545959,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:42:26', '2023-08-22 14:42:26', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143555793145036800, 1000000000000000001, '/tool/table/list', 621, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:42:27', '2023-08-22 14:42:27', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556251695710208, NULL, '/common/file/download', 206, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:44:17', '2023-08-22 14:44:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556262571540480, 1000000000000000001, '/tool/api/list', 11, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692686659244,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:44:19', '2023-08-22 14:44:19', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556269269843968, 1000000000000000001, '/tool/table/list', 593, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:44:21', '2023-08-22 14:44:21', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556500917059584, NULL, '/common/file/download', 204, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:45:16', '2023-08-22 14:45:16', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556509704126464, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692686718165,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:45:18', '2023-08-22 14:45:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556516687642624, 1000000000000000001, '/tool/table/list', 580, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:45:20', '2023-08-22 14:45:20', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556944485679104, NULL, '/common/file/download', 186, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:47:02', '2023-08-22 14:47:02', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556953578930176, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692686823993,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:47:04', '2023-08-22 14:47:04', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556959945883648, 1000000000000000001, '/tool/table/list', 605, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:47:06', '2023-08-22 14:47:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556983119413248, NULL, '/common/file/download', 185, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:47:11', '2023-08-22 14:47:11', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143556993982660608, 1000000000000000001, '/sys/user/list', 392, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:47:14', '2023-08-22 14:47:14', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143557240972640256, NULL, '/common/file/download', 170, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:48:13', '2023-08-22 14:48:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143557250187526144, 1000000000000000001, '/sys/user/list', 353, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:48:15', '2023-08-22 14:48:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143557465929940992, 1000000000000000001, '/sys/role/list', 285, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 14:49:06', '2023-08-22 14:49:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143557468597518336, 1000000000000000001, '/sys/menu/list', 198, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 14:49:07', '2023-08-22 14:49:07', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558622974836736, NULL, '/common/file/download', 180, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:53:42', '2023-08-22 14:53:42', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558632428797952, 1000000000000000001, '/sys/user/list', 354, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:53:44', '2023-08-22 14:53:44', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558637763952640, 1000000000000000001, '/sys/role/list', 263, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 14:53:46', '2023-08-22 14:53:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558640548970496, 1000000000000000001, '/sys/menu/list', 178, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 14:53:46', '2023-08-22 14:53:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558669397393408, 1000000000000000001, '/tool/api/list', 11, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687233076,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:53:53', '2023-08-22 14:53:53', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143558674673827840, 1000000000000000001, '/tool/table/list', 571, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 14:53:54', '2023-08-22 14:53:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143559990196305920, NULL, '/common/file/download', 553, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 14:59:08', '2023-08-22 14:59:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143559998907875328, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687550056,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 14:59:10', '2023-08-22 14:59:10', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560011083939840, 1000000000000000001, '/sys/user/list', 360, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 14:59:13', '2023-08-22 14:59:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560234027974656, NULL, '/common/file/download', 193, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:00:06', '2023-08-22 15:00:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560247181312000, 1000000000000000001, '/tool/api/list', 6, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687609249,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:00:09', '2023-08-22 15:00:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560253938335744, 1000000000000000001, '/tool/table/list', 601, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 15:00:11', '2023-08-22 15:00:11', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560268345769984, 1000000000000000001, '/sys/role/list', 284, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:00:14', '2023-08-22 15:00:14', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560274217795584, 1000000000000000001, '/sys/user/list', 373, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:00:16', '2023-08-22 15:00:16', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560288994328576, 1000000000000000001, '/sys/menu/list', 225, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 15:00:19', '2023-08-22 15:00:19', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560675553968128, NULL, '/common/file/download', 187, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:01:51', '2023-08-22 15:01:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560686048116736, 1000000000000000001, '/tool/api/list', 8, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687713883,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:01:54', '2023-08-22 15:01:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560692796751872, 1000000000000000001, '/tool/table/list', 603, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 15:01:55', '2023-08-22 15:01:55', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560702783389696, 1000000000000000001, '/sys/user/list', 354, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:01:58', '2023-08-22 15:01:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560705622933504, 1000000000000000001, '/sys/role/list', 232, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:01:59', '2023-08-22 15:01:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560708353425408, 1000000000000000001, '/sys/menu/list', 178, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 15:01:59', '2023-08-22 15:01:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560820911767552, NULL, '/common/file/download', 172, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:02:26', '2023-08-22 15:02:26', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560833062666240, 1000000000000000001, '/tool/table/list', 623, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 15:02:29', '2023-08-22 15:02:29', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560839983267840, 1000000000000000001, '/tool/api/list', 8, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687750585,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:02:31', '2023-08-22 15:02:31', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143560874967957504, 1000000000000000001, '/sys/user/list', 354, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:02:39', '2023-08-22 15:02:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561449403056128, NULL, '/common/file/download', 202, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:04:56', '2023-08-22 15:04:56', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561459272253440, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687898234,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:04:58', '2023-08-22 15:04:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561465899253760, 1000000000000000001, '/tool/table/list', 624, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 15:05:00', '2023-08-22 15:05:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561504566542336, 1000000000000000001, '/log/api/list', 275, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"fullName\":null,\"url\":\"\"}', NULL, '2023-08-22 15:05:09', '2023-08-22 15:05:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561514171498496, 1000000000000000001, '/sys/user/list', 357, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:05:11', '2023-08-22 15:05:11', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561518571323392, 1000000000000000001, '/sys/role/list', 265, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:05:12', '2023-08-22 15:05:12', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561520882384896, 1000000000000000001, '/sys/menu/list', 185, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 15:05:13', '2023-08-22 15:05:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561592558845952, NULL, '/common/file/download', 183, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:05:30', '2023-08-22 15:05:30', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561603690528768, 1000000000000000001, '/tool/api/list', 9, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687932666,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:05:33', '2023-08-22 15:05:33', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561609700966400, 1000000000000000001, '/tool/table/list', 573, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 15:05:34', '2023-08-22 15:05:34', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561617682726912, 1000000000000000001, '/sys/user/list', 365, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:05:36', '2023-08-22 15:05:36', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561620341915648, 1000000000000000001, '/sys/role/list', 230, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:05:37', '2023-08-22 15:05:37', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561623827382272, 1000000000000000001, '/sys/menu/list', 229, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 15:05:37', '2023-08-22 15:05:37', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561711815491584, NULL, '/common/file/download', 177, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:05:58', '2023-08-22 15:05:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561721604997120, 1000000000000000001, '/tool/api/list', 8, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692687960779,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 15:06:01', '2023-08-22 15:06:01', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561733546180608, 1000000000000000001, '/sys/user/list', 355, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:06:04', '2023-08-22 15:06:04', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561736536719360, 1000000000000000001, '/sys/role/list', 247, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:06:04', '2023-08-22 15:06:04', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561755272675328, 1000000000000000001, '/sys/menu/list', 224, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 15:06:09', '2023-08-22 15:06:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561916187148288, NULL, '/common/file/download', 177, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:06:47', '2023-08-22 15:06:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143561960839708672, NULL, '/common/file/download', 187, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:06:58', '2023-08-22 15:06:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143562269859250176, NULL, '/common/file/download', 175, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:08:11', '2023-08-22 15:08:11', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143562502781534208, NULL, '/common/file/download', 177, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:09:07', '2023-08-22 15:09:07', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143562536977694720, NULL, '/common/file/download', 177, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:09:15', '2023-08-22 15:09:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143563566662549504, NULL, '/common/file/download', 176, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:13:21', '2023-08-22 15:13:21', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143563763769671680, NULL, '/common/file/download', 178, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:14:08', '2023-08-22 15:14:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143563766353362944, NULL, '/common/file/download', 135, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:14:08', '2023-08-22 15:14:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143563806811619328, NULL, '/common/file/download', 182, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:14:18', '2023-08-22 15:14:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143563923610402816, NULL, '/common/file/download', 182, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:14:46', '2023-08-22 15:14:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143564135468892160, NULL, '/common/file/download', 176, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:15:36', '2023-08-22 15:15:36', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143564695018405888, NULL, '/common/file/download', 218, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:17:50', '2023-08-22 15:17:50', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143564889134989312, NULL, '/common/file/download', 184, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:18:36', '2023-08-22 15:18:36', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565040977182720, NULL, '/common/file/download', 178, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:19:12', '2023-08-22 15:19:12', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565094576193536, NULL, '/common/file/download', 173, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:19:25', '2023-08-22 15:19:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565171055132672, NULL, '/common/file/download', 186, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:19:43', '2023-08-22 15:19:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565279175901184, NULL, '/common/file/download', 185, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:20:09', '2023-08-22 15:20:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565366128017408, NULL, '/common/file/download', 185, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:20:30', '2023-08-22 15:20:30', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565444225957888, NULL, '/common/file/download', 177, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:20:48', '2023-08-22 15:20:48', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565529957531648, NULL, '/common/file/download', 183, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:21:09', '2023-08-22 15:21:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565599452954624, NULL, '/common/file/download', 180, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:21:25', '2023-08-22 15:21:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565645867122688, NULL, '/common/file/download', 187, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:21:36', '2023-08-22 15:21:36', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565648811524096, NULL, '/common/file/download', 175, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:21:37', '2023-08-22 15:21:37', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565735692337152, NULL, '/common/file/download', 182, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:21:58', '2023-08-22 15:21:58', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565809872797696, NULL, '/common/file/download', 198, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:22:16', '2023-08-22 15:22:16', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565816260722688, NULL, '/common/file/download', 190, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:22:17', '2023-08-22 15:22:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565817334464512, NULL, '/common/file/download', 132, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:22:17', '2023-08-22 15:22:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565818366263296, NULL, '/common/file/download', 132, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:22:18', '2023-08-22 15:22:18', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143565916282290176, NULL, '/common/file/download', 186, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:22:41', '2023-08-22 15:22:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143566198818996224, 1000000000000000001, '/sys/user/list', 353, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 15:23:48', '2023-08-22 15:23:48', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143566201906003968, 1000000000000000001, '/sys/role/list', 245, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 15:23:49', '2023-08-22 15:23:49', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143566930674712576, NULL, '/common/file/download', 197, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:26:43', '2023-08-22 15:26:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143566999499046912, NULL, '/common/file/download', 182, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:26:59', '2023-08-22 15:26:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143567027672186880, NULL, '/common/file/download', 180, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:27:06', '2023-08-22 15:27:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143567721598812160, NULL, '/common/file/download', 169, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:29:51', '2023-08-22 15:29:51', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143567796064485376, NULL, '/common/file/download', 195, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:30:09', '2023-08-22 15:30:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143568237636616192, NULL, '/common/file/download', 270, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:31:54', '2023-08-22 15:31:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143569264704225280, NULL, '/common/file/download', 173, '127.0.0.1', '\"1/202212/1050773458637553664.png\"', NULL, '2023-08-22 15:35:59', '2023-08-22 15:35:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143569292248219648, 1000000000000000001, '/logout', 3, '127.0.0.1', NULL, NULL, '2023-08-22 15:36:06', '2023-08-22 15:36:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143569302708813824, 1000000000000000001, '/login', 518, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 15:36:08', '2023-08-22 15:36:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143570340392206336, 1000000000000000001, '/logout', 0, '127.0.0.1', NULL, NULL, '2023-08-22 15:40:16', '2023-08-22 15:40:16', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143570681389121536, 1000000000000000001, '/login', 1537, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 15:41:37', '2023-08-22 15:41:37', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575630663647232, 1000000000000000001, '/tool/api/list', 41, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692691276950,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:01:17', '2023-08-22 16:01:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575650209103872, 1000000000000000001, '/sys/menu/list', 10239, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:01:22', '2023-08-22 16:01:22', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575660795527168, 1000000000000000001, '/tool/table/list', 5587, '127.0.0.1', '{\"ip\":\"47.98.194.238\",\"port\":\"3306\",\"schema\":\"mars\",\"user\":\"root\",\"password\":\"liankazdzq2022\"}', NULL, '2023-08-22 16:01:24', '2023-08-22 16:01:24', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575664939499520, 1000000000000000001, '/sys/user/list', 15439, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:01:25', '2023-08-22 16:01:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575666470420480, 1000000000000000001, '/sys/menu/list', 169, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:01:25', '2023-08-22 16:01:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575667590299648, 1000000000000000001, '/sys/role/list', 15292, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:01:26', '2023-08-22 16:01:26', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575669578399744, 1000000000000000001, '/sys/menu/list', 214, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:01:26', '2023-08-22 16:01:26', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143575670324985856, 1000000000000000001, '/sys/menu/list', 176, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:01:26', '2023-08-22 16:01:26', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143576870533464064, 1000000000000000001, '/logout', 1, '127.0.0.1', NULL, NULL, '2023-08-22 16:06:13', '2023-08-22 16:06:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143576879777710080, 1000000000000000001, '/login', 507, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:06:15', '2023-08-22 16:06:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143577503961448448, 1000000000000000001, '/login', 1007, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:08:44', '2023-08-22 16:08:44', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143577561645711360, 1000000000000000001, '/logout', 1, '127.0.0.1', NULL, NULL, '2023-08-22 16:08:57', '2023-08-22 16:08:57', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143577570986426368, 1000000000000000001, '/login', 529, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:09:00', '2023-08-22 16:09:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143578201604227072, 1000000000000000001, '/sys/role/list', 30071, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', 'Could not open JDBC Connection for transaction; nested exception is java.sql.SQLTransientConnectionException: HikariPool-2 - Connection is not available, request timed out after 30067ms.', '2023-08-22 16:11:30', '2023-08-22 16:11:30', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143578396064743424, 1000000000000000001, '/sys/menu/list', 223, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:12:16', '2023-08-22 16:12:16', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143578671940894720, 1000000000000000001, '/sys/menu/list', 219, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:13:22', '2023-08-22 16:13:22', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579010588999680, 1000000000000000001, '/sys/menu/list', 228, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:14:43', '2023-08-22 16:14:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579057003167744, 1000000000000000001, '/sys/role/list', 283, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:14:54', '2023-08-22 16:14:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579303858929664, 1000000000000000001, '/sys/role/list', 264, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:15:53', '2023-08-22 16:15:53', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579314868977664, 1000000000000000001, '/sys/user/list', 366, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:15:55', '2023-08-22 16:15:55', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579330421456896, 1000000000000000001, '/sys/menu/list', 221, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:15:59', '2023-08-22 16:15:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579341372784640, 1000000000000000001, '/sys/role/list', 288, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:16:02', '2023-08-22 16:16:02', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579347127369728, 1000000000000000001, '/sys/menu/list', 251, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:16:03', '2023-08-22 16:16:03', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579353108447232, 1000000000000000001, '/sys/menu/list', 219, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:16:04', '2023-08-22 16:16:04', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579373480181760, 1000000000000000001, '/sys/menu/list', 221, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:16:09', '2023-08-22 16:16:09', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579384708333568, 1000000000000000001, '/sys/menu/list', 231, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:16:12', '2023-08-22 16:16:12', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579390836211712, 1000000000000000001, '/sys/role/list', 261, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:16:13', '2023-08-22 16:16:13', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579400759934976, 1000000000000000001, '/sys/role/all', 216, '127.0.0.1', NULL, NULL, '2023-08-22 16:16:16', '2023-08-22 16:16:16', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143579420066316288, 1000000000000000001, '/sys/user/list', 361, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:16:20', '2023-08-22 16:16:20', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143579424621330432, 1000000000000000001, '/sys/role/all', 220, '127.0.0.1', NULL, NULL, '2023-08-22 16:16:22', '2023-08-22 16:16:22', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143579433664249856, 1000000000000000001, '/sys/user/list', 363, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:16:24', '2023-08-22 16:16:24', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582417529864192, 1000000000000000001, '/sys/menu/list', 1222, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:28:15', '2023-08-22 16:28:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582418289033216, 1000000000000000001, '/sys/menu/list', 205, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:28:15', '2023-08-22 16:28:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582533519147008, 1000000000000000001, '/sys/menu/list', 227, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:28:43', '2023-08-22 16:28:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582547620397056, 1000000000000000001, '/sys/menu/list', 232, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:28:46', '2023-08-22 16:28:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582605950582784, 1000000000000000001, '/sys/menu/add', 270, '127.0.0.1', '{\"menuName\":\"首页\",\"url\":\"/home/index\",\"parentId\":null,\"sort\":1,\"perms\":\"\",\"icon\":\"\",\"remark\":\"\"}', NULL, '2023-08-22 16:29:00', '2023-08-22 16:29:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582607972237312, 1000000000000000001, '/sys/menu/list', 178, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:29:00', '2023-08-22 16:29:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582640775888896, 1000000000000000001, '/logout', 3, '127.0.0.1', NULL, NULL, '2023-08-22 16:29:08', '2023-08-22 16:29:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582648048812032, 1000000000000000001, '/login', 398, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:29:10', '2023-08-22 16:29:10', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582702616707072, 1000000000000000001, '/sys/menu/list', 225, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:29:23', '2023-08-22 16:29:23', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582877414326272, 1000000000000000001, '/sys/menu/list', 229, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:30:05', '2023-08-22 16:30:05', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582877582098432, 1000000000000000001, '/sys/menu/get/1143582605195608064', 268, '127.0.0.1', '1143582605195608064', NULL, '2023-08-22 16:30:05', '2023-08-22 16:30:05', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582889401647104, 1000000000000000001, '/sys/menu/update', 262, '127.0.0.1', '{\"id\":1143582605195608064,\"menuName\":\"首页\",\"parentId\":0,\"sort\":1,\"url\":\"/home/index\",\"perms\":\"\",\"icon\":\"el-icon-s-home\",\"remark\":\"\"}', NULL, '2023-08-22 16:30:08', '2023-08-22 16:30:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582891737874432, 1000000000000000001, '/sys/menu/list', 256, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:30:08', '2023-08-22 16:30:08', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582927141994496, 1000000000000000001, '/logout', 0, '127.0.0.1', NULL, NULL, '2023-08-22 16:30:17', '2023-08-22 16:30:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582935719346176, 1000000000000000001, '/login', 382, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:30:19', '2023-08-22 16:30:19', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143582962499977216, 1000000000000000001, '/sys/user/list', 404, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:30:25', '2023-08-22 16:30:25', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583534535933952, 1000000000000000001, '/sys/user/list', 346, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:32:41', '2023-08-22 16:32:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583539539738624, 1000000000000000001, '/sys/role/list', 287, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:32:43', '2023-08-22 16:32:43', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583636323303424, 1000000000000000001, '/sys/menu/list', 215, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:33:06', '2023-08-22 16:33:06', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583655155728384, 1000000000000000001, '/sys/menu/list', 227, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:33:10', '2023-08-22 16:33:10', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583655260585984, 1000000000000000001, '/sys/menu/get/1143582605195608064', 252, '127.0.0.1', '1143582605195608064', NULL, '2023-08-22 16:33:10', '2023-08-22 16:33:10', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583670490103808, 1000000000000000001, '/sys/menu/update', 254, '127.0.0.1', '{\"id\":1143582605195608064,\"menuName\":\"首页\",\"parentId\":0,\"sort\":1,\"url\":\"\",\"perms\":\"\",\"icon\":\"el-icon-s-home\",\"remark\":\"\"}', NULL, '2023-08-22 16:33:14', '2023-08-22 16:33:14', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583672457232384, 1000000000000000001, '/sys/menu/list', 173, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:33:14', '2023-08-22 16:33:14', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583682930409472, 1000000000000000001, '/sys/menu/list', 218, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:33:17', '2023-08-22 16:33:17', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583808130383872, 1000000000000000001, '/sys/menu/add', 255, '127.0.0.1', '{\"menuName\":\"首页概览\",\"url\":\"/home/index\",\"parentId\":1143582605195608064,\"sort\":1,\"perms\":\"\",\"icon\":\"el-icon-s-platform\",\"remark\":\"\"}', NULL, '2023-08-22 16:33:47', '2023-08-22 16:33:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583810458222592, 1000000000000000001, '/sys/menu/list', 246, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:33:47', '2023-08-22 16:33:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583830465052672, 1000000000000000001, '/logout', 0, '127.0.0.1', NULL, NULL, '2023-08-22 16:33:52', '2023-08-22 16:33:52', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583839071764480, 1000000000000000001, '/login', 377, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:33:54', '2023-08-22 16:33:54', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583859393167360, 1000000000000000001, '/sys/user/list', 392, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"userName\":\"\",\"fullName\":\"\",\"phone\":\"\"}', NULL, '2023-08-22 16:33:59', '2023-08-22 16:33:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583862064939008, 1000000000000000001, '/sys/role/list', 259, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"roleName\":\"\"}', NULL, '2023-08-22 16:33:59', '2023-08-22 16:33:59', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143583864761876480, 1000000000000000001, '/sys/menu/list', 176, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:34:00', '2023-08-22 16:34:00', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584228680663040, 1000000000000000001, '/sys/menu/list', 244, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:35:27', '2023-08-22 16:35:27', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584263682129920, 1000000000000000001, '/sys/menu/list', 244, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":null}', NULL, '2023-08-22 16:35:35', '2023-08-22 16:35:35', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584274528600064, 1000000000000000001, '/sys/menu/list', 216, '127.0.0.1', '{\"pageNo\":1,\"pageSize\":10,\"menuName\":\"\"}', NULL, '2023-08-22 16:35:38', '2023-08-22 16:35:38', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584302160674816, 1000000000000000001, '/tool/api/list', 39, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693344395,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:44', '2023-08-22 16:35:44', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584311149068288, 1000000000000000001, '/tool/api/list', 7, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693346546,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:47', '2023-08-22 16:35:47', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143584313976029184, 1000000000000000001, '/tool/api/list', 6, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693347220,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:47', '2023-08-22 16:35:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584314902970368, 1000000000000000001, '/tool/api/list', 5, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693347441,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:47', '2023-08-22 16:35:47', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584322658238464, 1000000000000000001, '/tool/api/list', 5, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693349290,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:49', '2023-08-22 16:35:49', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_log` VALUES (1143584325879463936, 1000000000000000001, '/tool/api/list', 6, '127.0.0.1', '{\"swaggerUrl\":\"http://localhost:8080\",\"url\":\"\",\"title\":\"\"}', '404 : \"{\"timestamp\":1692693350058,\"status\":404,\"error\":\"Not Found\",\"message\":\"No message available\",\"path\":\"/v3/api-docs\"}\"', '2023-08-22 16:35:50', '2023-08-22 16:35:50', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143584355143122944, 1000000000000000001, '/logout', 0, '127.0.0.1', NULL, NULL, '2023-08-22 16:35:57', '2023-08-22 16:35:57', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143584362537680896, 1000000000000000001, '/login', 376, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"1\",\"captcha\":null}', NULL, '2023-08-22 16:35:59', '2023-08-22 16:35:59', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143584810644537344, 1000000000000000001, '/logout', 4, '127.0.0.1', NULL, NULL, '2023-08-22 16:37:46', '2023-08-22 16:37:46', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143584834975694848, NULL, '/login', 282, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"123456\",\"captcha\":null}', '密码错误', '2023-08-22 16:37:51', '2023-08-22 16:37:51', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143585595591753728, 1000000000000000001, '/login', 1443, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"123456\",\"captcha\":null}', NULL, '2023-08-22 16:40:53', '2023-08-22 16:40:53', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143585636561715200, 1000000000000000001, '/logout', 1, '127.0.0.1', NULL, NULL, '2023-08-22 16:41:03', '2023-08-22 16:41:03', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_log` VALUES (1143585680111173632, 1000000000000000001, '/login', 415, '127.0.0.1', '{\"userName\":\"admin\",\"pwd\":\"123456\",\"captcha\":null}', NULL, '2023-08-22 16:41:13', '2023-08-22 16:41:13', NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `menu_type` int NULL DEFAULT NULL COMMENT '1.目录 2 菜单 3 按钮',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'URL',
  `parent_id` bigint NOT NULL COMMENT '父ID',
  `sort` tinyint NOT NULL COMMENT '排序',
  `perms` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号ID',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号ID',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171101653676327178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1000000000000003001, '系统管理', 1, '', 0, 2, NULL, 'el-icon-setting', NULL, '2022-12-14 15:46:25', '2023-10-28 16:59:40', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003002, '接口文档', 1, NULL, 0, 10, NULL, 'el-icon-s-order', NULL, '2022-12-14 15:46:25', '2023-11-08 11:05:24', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1000000000000003003, '日志管理', 1, '', 0, 8, '', 'el-icon-document', '', '2022-12-14 15:46:25', '2023-11-08 11:06:33', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1000000000000003011, '用户管理', 2, 'user/user.html', 1000000000000003001, 1, NULL, 'el-icon-user-solid', NULL, '2022-12-14 15:46:25', '2023-11-14 23:09:38', NULL, NULL, NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1000000000000003012, '角色管理', 2, 'role/role.html', 1000000000000003001, 2, NULL, 'el-icon-s-cooperation', NULL, '2022-12-14 15:46:25', '2023-09-03 11:37:23', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003013, '菜单管理', 2, 'menu/menu.html', 1000000000000003001, 3, NULL, 'el-icon-s-order', NULL, '2022-12-14 15:46:25', '2023-09-03 11:37:44', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003021, '接口文档', 2, 'tool/api.html', 1000000000000003002, 1, NULL, 'el-icon-document', NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003031, '接口日志', 2, 'log/api.html', 1000000000000003003, 1, '', 'el-icon-document', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003111, '查询', 3, 'sys/user/list', 1000000000000003011, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003112, '新增', 3, 'sys/user/add', 1000000000000003011, 2, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003113, '修改', 3, 'sys/user/update', 1000000000000003011, 3, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003114, '删除', 3, 'sys/user/delete', 1000000000000003011, 4, NULL, NULL, NULL, '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003115, '导入', 3, 'sys/user/imports', 1000000000000003011, 5, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003116, '导出', 3, 'sys/user/export', 1000000000000003011, 6, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003211, '查询', 3, 'sys/role/list', 1000000000000003012, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003212, '新增', 3, 'sys/role/add', 1000000000000003012, 2, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003213, '修改', 3, 'sys/role/update', 1000000000000003012, 3, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003214, '删除', 3, 'sys/role/delete', 1000000000000003012, 4, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003311, '查询', 3, 'sys/menu/list', 1000000000000003013, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003312, '新增', 3, 'sys/menu/add', 1000000000000003013, 2, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003313, '修改', 3, 'sys/menu/update', 1000000000000003013, 3, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003314, '删除', 3, 'sys/menu/delete', 1000000000000003013, 4, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003421, '查询', 3, 'tool/api/list', 1000000000000003021, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003422, '导出', 3, 'tool/api/export', 1000000000000003021, 2, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003521, '查询', 3, 'tool/table/list', 1000000000000003022, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1000000000000003522, '导出', 3, 'tool/table/export', 1000000000000003022, 2, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1143582605195608064, '首页', 1, '', 0, 1, '', 'el-icon-s-home', '', '2023-08-22 16:29:00', '2023-11-08 11:04:46', NULL, NULL, 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1143583807417352192, '首页概览', 2, 'home/home.html', 1143582605195608064, 1, '', 'el-icon-s-platform', '', '2023-08-22 16:33:46', '2023-08-22 16:33:46', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1164976341993390080, '数据采集', 1, '', 0, 5, '', 'el-icon-s-order', '', '2023-10-20 17:20:04', '2023-10-20 23:17:15', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1165064540677734400, '资源列表', 2, 'info/info.html', 1164976341993390080, 2, '', 'el-icon-s-order', '', '2023-10-20 23:10:32', '2023-10-20 23:22:39', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682368, '岗位管理', 2, 'post/post.html', 1000000000000003001, 4, '', 'el-icon-s-ticket', '', '2023-11-03 23:22:52', '2023-11-08 12:18:59', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1170141074442682369, '查询', 3, 'sys/post/pageList', 1170141074442682368, 1, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682370, '新增', 3, 'sys/post/add', 1170141074442682368, 2, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682371, '修改', 3, 'sys/post/update', 1170141074442682368, 3, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1170141074442682372, '删除', 3, 'sys/post/delete', 1170141074442682368, 4, '', '', '', '2022-12-14 15:46:25', '2022-12-14 15:46:41', NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676326936, '代码部署', 1, '', 0, 12, '', 'el-icon-s-promotion', '', '2023-11-07 15:24:44', '2023-11-08 11:03:04', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676326938, '代码生成', 2, 'gen/import-table.html', 1171101653676326936, 1, '', 'el-icon-s-promotion', '', '2023-11-07 17:58:29', '2023-11-08 14:16:28', 1000000000000000001, 'admin', 1000000000000000001, 'admin', 0);
INSERT INTO `sys_menu` VALUES (1171101653676327124, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327125, '教程列表', 2, 'starCource/starCource.html', 1171101653676327124, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327126, '教程添加', 3, 'admin/starCource/add', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327127, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327128, '教程修改', 3, 'admin/starCource/update', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327129, '教程删除', 3, 'admin/starCource/delete', 1171101653676327125, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 16:27:24', '2023-11-10 17:19:22', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327136, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327137, '教程列表', 2, 'starCource/starCource.html', 1171101653676327136, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327138, '教程添加', 3, 'admin/starCource/add', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327139, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327140, '教程修改', 3, 'admin/starCource/update', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327141, '教程删除', 3, 'admin/starCource/delete', 1171101653676327137, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:21:25', '2023-11-13 13:54:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327142, '班级管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327143, '班级列表', 2, 'classInfo/classInfo.html', 1171101653676327142, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327144, '班级添加', 3, 'admin/classInfo/add', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327145, '班级列表', 3, 'admin/classInfo/pageList', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327146, '班级修改', 3, 'admin/classInfo/update', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327147, '班级删除', 3, 'admin/classInfo/delete', 1171101653676327143, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-10 17:22:03', '2023-11-13 13:55:05', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327148, '教程列表', 2, 'starCource/starCource.html', 1000000000000003001, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327149, '教程添加', 3, 'admin/starCource/add', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327150, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327151, '教程修改', 3, 'admin/starCource/update', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327152, '教程删除', 3, 'admin/starCource/delete', 1171101653676327148, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 14:23:43', '2023-11-13 16:18:20', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327153, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327154, '教程列表', 2, 'starCource/starCource.html', 1171101653676327153, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327155, '教程添加', 3, 'admin/starCource/add', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327156, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327157, '教程修改', 3, 'admin/starCource/update', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327158, '教程删除', 3, 'admin/starCource/delete', 1171101653676327154, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:27:13', '2023-11-13 20:29:09', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327159, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:41', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327160, '教程列表', 2, 'starCource/starCource.html', 1171101653676327159, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327161, '教程添加', 3, 'admin/starCource/add', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327162, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327163, '教程修改', 3, 'admin/starCource/update', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327164, '教程删除', 3, 'admin/starCource/delete', 1171101653676327160, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 20:43:42', '2023-11-13 21:35:07', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327165, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:29', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327166, '教程列表', 2, 'starCource/starCource.html', 1171101653676327165, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:29', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327167, '教程添加', 3, 'admin/starCource/add', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327168, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327169, '教程修改', 3, 'admin/starCource/update', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327170, '教程删除', 3, 'admin/starCource/delete', 1171101653676327166, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:37:30', '2023-11-13 21:38:39', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327171, '教程管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:40:59', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327172, '教程列表', 2, 'starCource/starCource.html', 1171101653676327171, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:40:59', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327173, '教程添加', 3, 'admin/starCource/add', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327174, '教程列表', 3, 'admin/starCource/pageList', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327175, '教程修改', 3, 'admin/starCource/update', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327176, '教程删除', 3, 'admin/starCource/delete', 1171101653676327172, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-13 21:41:00', '2023-11-13 21:41:50', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327177, '快速建表', 2, 'gen/create-table.html', 1171101653676326936, 1, '', 'el-icon-s-order', '', '2023-11-13 21:46:23', '2023-11-13 21:46:22', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327178, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327179, '战队列表', 2, 'tTeam/tTeam.html', 1171101653676327178, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327180, '战队添加', 3, 'admin/tTeam/add', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327181, '战队列表', 3, 'admin/tTeam/pageList', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327182, '战队修改', 3, 'admin/tTeam/update', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327183, '战队删除', 3, 'admin/tTeam/delete', 1171101653676327179, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:37:24', '2023-11-14 22:42:12', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327184, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:32', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327185, '战队列表', 2, 'apTeam/apTeam.html', 1171101653676327184, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:32', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327186, '战队添加', 3, 'admin/apTeam/add', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327187, '战队列表', 3, 'admin/apTeam/pageList', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327188, '战队修改', 3, 'admin/apTeam/update', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327189, '战队删除', 3, 'admin/apTeam/delete', 1171101653676327185, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:45:33', '2023-11-14 22:47:27', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327190, '战队管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:23', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327191, '战队列表', 2, 'apTeam/apTeam.html', 1171101653676327190, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:23', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327192, '战队添加', 3, 'admin/apTeam/add', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327193, '战队列表', 3, 'admin/apTeam/pageList', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327194, '战队修改', 3, 'admin/apTeam/update', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327195, '战队删除', 3, 'admin/apTeam/delete', 1171101653676327191, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 22:51:24', '2023-11-14 23:10:02', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327196, '用户管理', 1, '#', 0, 14, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327197, '用户列表', 2, 'apUser/apUser.html', 1171101653676327196, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327198, '用户添加', 3, 'admin/apUser/add', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327199, '用户列表', 3, 'admin/apUser/pageList', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327200, '用户修改', 3, 'admin/apUser/update', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327201, '用户删除', 3, 'admin/apUser/delete', 1171101653676327197, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:00:17', '2023-11-14 23:09:38', 1000000000000000001, 'admin', NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES (1171101653676327202, '用户管理', 1, '#', 0, 13, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327203, '用户列表', 2, 'apUser/apUser.html', 1171101653676327202, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327204, '用户添加', 3, 'admin/apUser/add', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327205, '用户列表', 3, 'admin/apUser/pageList', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327206, '用户修改', 3, 'admin/apUser/update', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);
INSERT INTO `sys_menu` VALUES (1171101653676327207, '用户删除', 3, 'admin/apUser/delete', 1171101653676327203, 1, NULL, 'el-icon-notebook-2', NULL, '2023-11-14 23:11:00', '2023-11-14 23:11:00', 1000000000000000001, 'admin', NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `post_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `post_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '岗位编码',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, '董事长', '00002', '董事长', NULL, NULL, '2023-11-03 23:43:36', 1000000000000000001, 'admin', '2023-11-07 16:02:27', 0);
INSERT INTO `sys_post` VALUES (2, '技术总监', 'yuanmazijie-002', NULL, 1000000000000000001, 'admin', '2023-11-03 23:54:38', NULL, NULL, '2023-11-03 23:58:30', 1);
INSERT INTO `sys_post` VALUES (3, '测试岗位', 'yuanmazjie-003', NULL, 1000000000000000001, 'admin', '2023-11-03 23:56:16', NULL, NULL, '2023-11-03 23:58:26', 1);
INSERT INTO `sys_post` VALUES (4, '总经理', 'yuanmajie-002', '总经理111', 1000000000000000001, 'admin', '2023-11-03 23:59:06', 1000000000000000001, 'admin', '2023-11-04 00:09:11', 1);
INSERT INTO `sys_post` VALUES (5, '测试岗位', '0001', '11111', 1000000000000000001, 'admin', '2023-11-04 00:13:34', 1000000000000000001, 'admin', '2023-11-07 16:02:15', 0);
INSERT INTO `sys_post` VALUES (6, '22', '111', '21212', 1000000000000000001, 'admin', '2023-11-04 21:45:38', NULL, NULL, '2023-11-04 21:45:43', 1);
INSERT INTO `sys_post` VALUES (7, '1212', '1212', '2222', 1000000000000000001, 'admin', '2023-11-04 21:46:04', NULL, NULL, '2023-11-04 21:46:29', 1);
INSERT INTO `sys_post` VALUES (8, '2122', '212', '1212', 1000000000000000001, 'admin', '2023-11-04 21:46:23', NULL, NULL, '2023-11-04 21:46:32', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `role_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色key',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1170050863721349121 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1052613170658541568, '超级管理员', 'super_admin', '拥有所有权限', NULL, NULL, '2022-12-14 15:48:56', 1000000000000000001, 'admin', '2022-12-14 15:48:56', 0);
INSERT INTO `sys_role` VALUES (1052613468324102144, '普通用户', NULL, '查看数据采集和日志', NULL, NULL, '2022-12-14 15:50:07', 1170035678893834240, 'zhangsan', '2022-12-14 15:50:07', 0);
INSERT INTO `sys_role` VALUES (1170027725386153984, '测试角色', NULL, '111', NULL, NULL, '2023-11-03 15:52:28', NULL, NULL, '2023-11-03 15:52:28', 1);
INSERT INTO `sys_role` VALUES (1170029865148088320, '21212', NULL, '21212', NULL, NULL, '2023-11-03 16:00:58', NULL, NULL, '2023-11-03 16:00:58', 1);
INSERT INTO `sys_role` VALUES (1170034943355518976, '1212222', NULL, '212', 1000000000000000001, 'admin', '2023-11-03 16:21:09', 1000000000000000001, 'admin', '2023-11-03 16:21:09', 1);
INSERT INTO `sys_role` VALUES (1170048879480012800, '测试角色', NULL, '测试角色', 1000000000000000001, 'admin', '2023-11-03 17:16:31', NULL, NULL, '2023-11-03 17:16:31', 1);
INSERT INTO `sys_role` VALUES (1170050863721349120, '测试角色', NULL, '测试人员', 1000000000000000001, 'admin', '2023-11-03 17:24:24', 1000000000000000001, 'admin', '2023-11-03 17:24:24', 0);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色ID',
  `menu_id` bigint UNSIGNED NOT NULL COMMENT '菜单ID',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171412282094126335 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色关联菜单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1170036606397054976, 1052613468324102144, 1143582605195608064, '2023-11-03 16:27:45', '2023-11-03 16:27:45');
INSERT INTO `sys_role_menu` VALUES (1170036606782930944, 1052613468324102144, 1143583807417352192, '2023-11-03 16:27:45', '2023-11-03 16:27:45');
INSERT INTO `sys_role_menu` VALUES (1170036607173001216, 1052613468324102144, 1164976341993390080, '2023-11-03 16:27:45', '2023-11-03 16:27:45');
INSERT INTO `sys_role_menu` VALUES (1170036607558877184, 1052613468324102144, 1165064540677734400, '2023-11-03 16:27:45', '2023-11-03 16:27:45');
INSERT INTO `sys_role_menu` VALUES (1171412282094125242, 1170050863721349120, 1164976341993390080, '2023-11-08 12:19:29', '2023-11-08 12:19:29');
INSERT INTO `sys_role_menu` VALUES (1171412282094125243, 1170050863721349120, 1165064540677734400, '2023-11-08 12:19:29', '2023-11-08 12:19:29');
INSERT INTO `sys_role_menu` VALUES (1171412282094126301, 1052613170658541568, 1143582605195608064, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126302, 1052613170658541568, 1143583807417352192, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126303, 1052613170658541568, 1000000000000003001, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126304, 1052613170658541568, 1000000000000003011, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126305, 1052613170658541568, 1000000000000003111, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126306, 1052613170658541568, 1000000000000003112, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126307, 1052613170658541568, 1000000000000003113, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126308, 1052613170658541568, 1000000000000003114, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126309, 1052613170658541568, 1000000000000003115, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126310, 1052613170658541568, 1000000000000003116, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126311, 1052613170658541568, 1000000000000003012, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126312, 1052613170658541568, 1000000000000003211, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126313, 1052613170658541568, 1000000000000003212, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126314, 1052613170658541568, 1000000000000003213, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126315, 1052613170658541568, 1000000000000003214, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126316, 1052613170658541568, 1000000000000003013, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126317, 1052613170658541568, 1000000000000003311, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126318, 1052613170658541568, 1000000000000003312, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126319, 1052613170658541568, 1000000000000003313, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126320, 1052613170658541568, 1000000000000003314, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126321, 1052613170658541568, 1170141074442682368, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126322, 1052613170658541568, 1170141074442682369, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126323, 1052613170658541568, 1170141074442682370, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126324, 1052613170658541568, 1170141074442682371, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126325, 1052613170658541568, 1170141074442682372, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126326, 1052613170658541568, 1000000000000003003, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126327, 1052613170658541568, 1000000000000003031, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126328, 1052613170658541568, 1000000000000003002, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126329, 1052613170658541568, 1000000000000003021, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126330, 1052613170658541568, 1000000000000003421, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126331, 1052613170658541568, 1000000000000003422, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126332, 1052613170658541568, 1171101653676326936, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126333, 1052613170658541568, 1171101653676327177, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126334, 1052613170658541568, 1171101653676326938, '2023-11-13 21:46:58', '2023-11-13 21:46:58');
INSERT INTO `sys_role_menu` VALUES (1171412282094126335, 1052613170658541568, 1171101653676327178, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126336, 1052613170658541568, 1171101653676327179, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126337, 1052613170658541568, 1171101653676327180, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126338, 1052613170658541568, 1171101653676327181, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126339, 1052613170658541568, 1171101653676327182, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126340, 1052613170658541568, 1171101653676327183, '2023-11-14 22:37:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126341, 1052613170658541568, 1171101653676327178, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126342, 1052613170658541568, 1171101653676327179, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126343, 1052613170658541568, 1171101653676327180, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126344, 1052613170658541568, 1171101653676327181, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126345, 1052613170658541568, 1171101653676327182, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126346, 1052613170658541568, 1171101653676327183, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126347, 1052613170658541568, 1171101653676327184, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126348, 1052613170658541568, 1171101653676327185, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126349, 1052613170658541568, 1171101653676327186, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126350, 1052613170658541568, 1171101653676327187, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126351, 1052613170658541568, 1171101653676327188, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126352, 1052613170658541568, 1171101653676327189, '2023-11-14 22:45:33', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126353, 1052613170658541568, 1171101653676327178, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126354, 1052613170658541568, 1171101653676327179, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126355, 1052613170658541568, 1171101653676327180, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126356, 1052613170658541568, 1171101653676327181, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126357, 1052613170658541568, 1171101653676327182, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126358, 1052613170658541568, 1171101653676327183, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126359, 1052613170658541568, 1171101653676327184, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126360, 1052613170658541568, 1171101653676327185, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126361, 1052613170658541568, 1171101653676327186, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126362, 1052613170658541568, 1171101653676327187, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126363, 1052613170658541568, 1171101653676327188, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126364, 1052613170658541568, 1171101653676327189, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126365, 1052613170658541568, 1171101653676327190, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126366, 1052613170658541568, 1171101653676327191, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126367, 1052613170658541568, 1171101653676327192, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126368, 1052613170658541568, 1171101653676327193, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126369, 1052613170658541568, 1171101653676327194, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126370, 1052613170658541568, 1171101653676327195, '2023-11-14 22:51:24', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126371, 1052613170658541568, 1000000000000003011, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126372, 1052613170658541568, 1171101653676327196, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126373, 1052613170658541568, 1171101653676327197, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126374, 1052613170658541568, 1171101653676327198, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126375, 1052613170658541568, 1171101653676327199, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126376, 1052613170658541568, 1171101653676327200, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126377, 1052613170658541568, 1171101653676327201, '2023-11-14 23:00:18', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126378, 1052613170658541568, 1000000000000003011, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126379, 1052613170658541568, 1171101653676327196, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126380, 1052613170658541568, 1171101653676327197, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126381, 1052613170658541568, 1171101653676327198, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126382, 1052613170658541568, 1171101653676327199, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126383, 1052613170658541568, 1171101653676327200, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126384, 1052613170658541568, 1171101653676327201, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126385, 1052613170658541568, 1171101653676327202, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126386, 1052613170658541568, 1171101653676327203, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126387, 1052613170658541568, 1171101653676327204, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126388, 1052613170658541568, 1171101653676327205, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126389, 1052613170658541568, 1171101653676327206, '2023-11-14 23:11:01', NULL);
INSERT INTO `sys_role_menu` VALUES (1171412282094126390, 1052613170658541568, 1171101653676327207, '2023-11-14 23:11:01', NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `real_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `sex` tinyint NOT NULL COMMENT '性别(1男  2女)',
  `birth_date` date NOT NULL COMMENT '出生日期',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号码',
  `type` tinyint NOT NULL DEFAULT 0 COMMENT '用户类型(0内置用户 1注册用户)',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `create_by_id` bigint NULL DEFAULT NULL COMMENT '创建人账号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `update_by_id` bigint NULL DEFAULT NULL COMMENT '更新人账号',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新人名称',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171489978102841345 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1000000000000000001, 'admin', '$2a$10$Hzu6JmlVjQPDKA6Gdn8XXO/vmyU7ER9NUEppMRK5TcJdshHW.Fsq6', '管理员', 1, '2020-12-14', '13000000000', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', NULL, '2020-12-14 09:57:51', NULL, NULL, '2020-12-14 09:57:54', NULL, 0);
INSERT INTO `sys_user` VALUES (1167805277462855680, 'mars', '$2a$10$jIzbnaFLxA2UXgE.oAaShOronaqNOpguf0ZAG.wKqW2sjB0SH7y7W', '程序员Mars', 1, '2023-10-27', '18888888888', 1, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', NULL, '2023-10-28 12:41:15', NULL, NULL, '2023-10-28 12:41:15', NULL, 0);
INSERT INTO `sys_user` VALUES (1170035678893834240, 'zhangsan', '$2a$10$sYk0NHkdQ08G7rZk38K9y.LI6TTUfzV49A2/kofo2XehHbg.cq6xy', '张三', 1, '2023-11-02', '18483678311', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-03 16:24:04', 'admin', 1000000000000000001, '2023-11-03 16:24:04', 'admin', 1);
INSERT INTO `sys_user` VALUES (1170050787619897344, 'lisi', '$2a$10$TdWgsUeDPA7.72rGL0YWMegoT2b9.TF/8m4Vh6W6vrrV0VvCma4lq', '李四', 1, '2023-11-02', '18483678323', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-03 17:24:06', 'admin', 1000000000000000001, '2023-11-03 17:24:06', 'admin', 0);
INSERT INTO `sys_user` VALUES (1171489978102841344, 'wangwu', '$2a$10$Awurz12q516iFaf2wYP0/On1uTpusixlsh1i3vaymHQV6s3AT5HCe', '王五', 1, '2023-11-07', '18483688369', 0, 'https://foruda.gitee.com/avatar/1692522394185109890/4768152_marsfactory_1692522394.png!avatar200', '成都市', 1000000000000000001, '2023-11-07 16:42:56', 'admin', 1000000000000000001, '2023-11-07 16:42:56', 'admin', 0);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (2, 1171489978102841344, 1, '2023-11-07 17:02:21', '2023-11-07 17:02:21');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1171494864911597569 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户关联角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (2, 1000000000000000001, 1052613170658541568, '2023-10-28 12:45:29', '2023-10-28 12:45:32');
INSERT INTO `sys_user_role` VALUES (1052613759698206720, 1052613759316525056, 1052613468324102144, '2022-12-14 15:51:17', '2022-12-14 15:51:17');
INSERT INTO `sys_user_role` VALUES (1052613787443527680, 1052613658141523968, 1052613170658541568, '2022-12-14 15:51:24', '2022-12-14 15:51:24');
INSERT INTO `sys_user_role` VALUES (1167407043858923520, 1052613759316525091, 1052613170658541568, '2023-10-27 10:18:49', '2023-10-27 10:18:49');
INSERT INTO `sys_user_role` VALUES (1167876010381672448, 1167805277462855680, 1052613468324102144, '2023-10-28 17:22:19', '2023-10-28 17:22:19');
INSERT INTO `sys_user_role` VALUES (1170050908587819008, 1170050787619897344, 1170050863721349120, '2023-11-03 17:24:35', '2023-11-03 17:24:35');
INSERT INTO `sys_user_role` VALUES (1171494864911597568, 1171489978102841344, 1052613468324102144, '2023-11-07 17:02:21', '2023-11-07 17:02:21');

-- ----------------------------
-- Table structure for work_base_user
-- ----------------------------
DROP TABLE IF EXISTS `work_base_user`;
CREATE TABLE `work_base_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人账号',
  `create_by_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人名称',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人账号',
  `update_by_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '修改人名称',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `org_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '工厂代码',
  `org_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '工厂名称',
  `region_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区域代码',
  `region_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区域名称',
  `del_flag` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除标识：false-未删除，true-已删除',
  `real_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '姓名',
  `ldap` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'LDAP帐号',
  `is_admin` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '员工岗位属性；1-作业人员，2-外来作业人员',
  `sex` tinyint UNSIGNED NULL DEFAULT 3 COMMENT '性别 1-男 2-女',
  `id_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '身份证',
  `birth_date` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '出生日期',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机',
  `uploaded` bit(1) NULL DEFAULT b'1' COMMENT '是否必传安全分分析和培训记录',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '是否启用',
  `company_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '所属单位id',
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属单位名称',
  `department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属部门名称',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unq_user`(`ldap` ASC, `del_flag` ASC) USING BTREE,
  INDEX `pk_id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '作业票模块-员工信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of work_base_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
