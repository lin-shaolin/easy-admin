package ${packageName}.controller;


import java.util.Arrays;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import ${packageName}.service.I${ClassName}Service;
import ${packageName}.request.${ClassName}Request;
#if($table.crud)
#elseif($table.tree)
#end

/**
 * ${functionName}控制层
 *
 * @author ${author}
 * @date ${datetime}
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "${functionName}接口管理",tags = "${functionName}接口管理")
@RequestMapping("/${moduleName}/${jsName}" )
public class ${ClassName}Controller {

    private final I${ClassName}Service i${ClassName}Service;

    /**
     * 分页查询${functionName}列表
     */
    @ApiOperation(value = "分页查询${functionName}列表")
    @PostMapping("/pageList")
    public R list(@RequestBody ${ClassName}Request ${className}) {
        return R.success(i${ClassName}Service.pageList(${className}));
    }

    /**
     * 获取${functionName}详细信息
     */
    @ApiOperation(value = "获取${functionName}详细信息")
    @GetMapping(value = "/query/{${pkColumn.javaField}}" )
    public R getInfo(@PathVariable("${pkColumn.javaField}" ) ${pkColumn.javaType} ${pkColumn.javaField}) {
        return R.success(i${ClassName}Service.getById(${pkColumn.javaField}));
    }

    /**
     * 新增${functionName}
     */
    @ApiOperation(value = "新增${functionName}")
    @PostMapping("/add")
    public R add(@RequestBody ${ClassName}Request ${className}) {
        i${ClassName}Service.add(${className});
        return R.success();
    }

    /**
     * 修改${functionName}
     */
    @ApiOperation(value = "修改${functionName}")
    @PostMapping("/update")
    public R edit(@RequestBody ${ClassName}Request ${className}) {
        i${ClassName}Service.update(${className});
        return R.success();
    }

    /**
     * 删除${functionName}
     */
    @ApiOperation(value = "删除${functionName}")
    @PostMapping("/delete/{${pkColumn.javaField}s}" )
    public R remove(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        i${ClassName}Service.deleteBatch(Arrays.asList(${pkColumn.javaField}s));
        return R.success();
    }
}
