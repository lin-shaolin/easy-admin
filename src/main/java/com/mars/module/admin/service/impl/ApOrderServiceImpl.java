package com.mars.module.admin.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApOrderRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApOrderMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApOrderService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 订单业务层处理
 *
 * @author mars
 * @date 2023-11-16
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApOrderServiceImpl implements IApOrderService {

    private final ApOrderMapper baseMapper;

    @Override
    public ApOrder add(ApOrderRequest request) {
        ApOrder entity = new ApOrder();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    public boolean delete(Integer id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public boolean deleteBatch(List<Integer> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean update(ApOrderRequest request) {
        ApOrder entity = new ApOrder();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApOrder getById(Integer id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApOrder> pageList(ApOrderRequest request) {
        Page<ApOrder> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApOrder> query = this.buildWrapper(request);
        IPage<ApOrder> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    @Override
    public List<ApOrder> getOrderList(ApOrderRequest request) {
        LambdaQueryWrapper<ApOrder> wrapper = this.buildWrapper(request);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void importOrder(MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RuntimeException("请选择文件");
        }
        ImportParams params = new ImportParams();
        params.setTitleRows(1);
        List<ApOrder> result = ExcelImportUtil.importExcel(file.getInputStream(), ApOrder.class, params);
        baseMapper.insertBatchSomeColumn(result);
    }

    private LambdaQueryWrapper<ApOrder> buildWrapper(ApOrderRequest param) {
        LambdaQueryWrapper<ApOrder> query = new LambdaQueryWrapper<>();
        return query;
    }

}
