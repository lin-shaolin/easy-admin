package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import com.mars.module.admin.entity.ApOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApOrderService;
import com.mars.module.admin.request.ApOrderRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 订单控制层
 *
 * @author mars
 * @date 2023-11-16
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "订单接口管理", tags = "订单接口管理")
@RequestMapping("/admin/apOrder")
public class ApOrderController {

    private final IApOrderService iApOrderService;

    /**
     * 分页查询订单列表
     */
    @ApiOperation(value = "分页查询订单列表")
    @PostMapping("/pageList")
    public R list(@RequestBody ApOrderRequest apOrder) {
        return R.success(iApOrderService.pageList(apOrder));
    }

    /**
     * 获取订单详细信息
     */
    @ApiOperation(value = "获取订单详细信息")
    @GetMapping(value = "/query/{id}")
    public R getInfo(@PathVariable("id") Integer id) {
        return R.success(iApOrderService.getById(id));
    }

    /**
     * 新增订单
     */
    @ApiOperation(value = "新增订单")
    @PostMapping("/add")
    public R add(@RequestBody ApOrderRequest apOrder) {
        iApOrderService.add(apOrder);
        return R.success();
    }

    /**
     * 修改订单
     */
    @ApiOperation(value = "修改订单")
    @PostMapping("/update")
    public R edit(@RequestBody ApOrderRequest apOrder) {
        iApOrderService.update(apOrder);
        return R.success();
    }

    /**
     * 删除订单
     */
    @ApiOperation(value = "删除订单")
    @PostMapping("/delete/{ids}")
    public R remove(@PathVariable Integer[] ids) {
        iApOrderService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
     * 导出数据
     *
     * @param response response
     * @throws IOException IOException
     */
    @ApiOperation(value = "导出订单")
    @GetMapping(value = "/export")
    public void exportExcel(ApOrderRequest request, HttpServletResponse response) throws IOException {
        List<ApOrder> userList = iApOrderService.getOrderList(request);
        ExcelUtils.exportExcel(userList, ApOrder.class, "订单信息", response);
    }

    /**
     * 导入excel数据
     *
     * @param file file
     */
    @ApiOperation(value = "导入订单")
    @PostMapping("/import")
    public void importExcel(@RequestParam("file") MultipartFile file) throws Exception {
        iApOrderService.importOrder(file);
    }


}
