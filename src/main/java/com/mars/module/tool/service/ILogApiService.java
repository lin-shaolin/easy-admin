package com.mars.module.tool.service;

import com.mars.common.request.log.LogApiQueryRequest;
import com.mars.common.response.log.LogApiResponse;
import com.mars.common.response.PageInfo;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-23 10:07:21
 */
public interface ILogApiService {

    /**
     * 分页列表
     *
     * @param queryDto queryDto
     * @return PageVo<LogApiResponse>
     */
    PageInfo<LogApiResponse> list(LogApiQueryRequest queryDto);

    /**
     * 删除日志
     *
     * @param id id
     */
    void delete(Long id);
}
