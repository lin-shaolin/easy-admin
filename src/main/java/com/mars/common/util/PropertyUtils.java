package com.mars.common.util;

import org.springframework.core.env.Environment;

/**
 * Spring Environment，用于静态获取配置参数
 *
 * @author 源码字节-程序员Mars
 */
public class PropertyUtils {

    private static Environment environment = SpringContextUtils.getBean(Environment.class);

    public static String getValue(String name) {
        return environment.getProperty(name);
    }

    public static String getDbIp() {
        String url = environment.getProperty("spring.datasource.url");
        return url.substring(url.indexOf("//") + 2, url.lastIndexOf(":"));
    }

    public static String getDbPort() {
        String url = environment.getProperty("spring.datasource.url");
        String temp = url.substring(0, url.indexOf("?"));
        return temp.substring(temp.lastIndexOf(":") + 1, temp.lastIndexOf("/"));
    }

    public static String getDbSchema() {
        String url = environment.getProperty("spring.datasource.url");
        String temp = url.substring(0, url.indexOf("?"));
        return temp.substring(temp.lastIndexOf("/") + 1);
    }

    public static String getDbUser() {
        return environment.getProperty("spring.datasource.username");
    }

    public static String getDbPassword() {
        return environment.getProperty("spring.datasource.password");
    }
}
