package com.mars.common.request.sys;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRoleQueryRequest extends PageRequest {

    @ApiModelProperty(value = "名称")
    private String roleName;


}
