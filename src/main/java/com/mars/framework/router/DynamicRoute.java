package com.mars.framework.router;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-07 17:27:35
 */
@Slf4j
@Component
public class DynamicRoute {

    private final ConcurrentHashMap<String, String> ROUTE_MAP = new ConcurrentHashMap<>();

    /**
     * 初始化路由
     */
    @PostConstruct
    public void init() {
        ROUTE_MAP.put("home/home.html", "home/home");
        ROUTE_MAP.put("info/info.html", "info/info");
        ROUTE_MAP.put("log/api.html", "log/api");
        ROUTE_MAP.put("menu/menu.html", "menu/menu");
        ROUTE_MAP.put("menu/menuAdd.html", "menu/menuAdd");
        ROUTE_MAP.put("menu/menuUpdate.html", "menu/menuUpdate");
        ROUTE_MAP.put("role/role", "role/role");
        ROUTE_MAP.put("role/roleAdd.html", "role/roleAdd");
        ROUTE_MAP.put("role/roleUpdate.html", "role/roleUpdate");
        ROUTE_MAP.put("tool/api.html", "tool/api");
        ROUTE_MAP.put("gen/import-table.html", "gen/table");
        ROUTE_MAP.put("user/user.html", "user/user");
        ROUTE_MAP.put("user/userAdd.html", "user/userAdd");
        ROUTE_MAP.put("user/userUpdate.html", "user/userUpdate");
        ROUTE_MAP.put("user/userImport.html", "user/userImport");
        ROUTE_MAP.put("post/post.html", "post/post");
        ROUTE_MAP.put("post/post-add-update.html", "post/post-add-update");
        ROUTE_MAP.put("course/index.html", "course/index");
        ROUTE_MAP.put("course/index-add-update.html", "course/index-add-update");
        log.info("Easy-Admin动态路由初始化完成~");
    }


    /**
     * 获取路由
     *
     * @param key key
     * @return 路由地址
     */
    public String getRoute(String key) {
        return ROUTE_MAP.get(key);
    }

    public static void main(String[] args) {
        writeRouter("tes2");
    }

    /**
     * 根据功能名称写入路由
     *
     * @param businessName businessName
     */
    public static void writeRouter(String businessName) {
        String dir = System.getProperty("user.dir");
        File file = new File(dir + "\\src\\main\\java\\com\\mars\\router\\RouterController.java");
        try {
            // 读取原文件内容
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder fileContent = new StringBuilder();
            String line;
            String lastLine = null;
            while ((line = reader.readLine()) != null) {
                if (lastLine != null) {
                    fileContent.append(lastLine).append(System.lineSeparator());
                }
                lastLine = line;
            }
            reader.close();
            int lastBraceIndex = lastLine.lastIndexOf("}");
            if (lastBraceIndex >= 0) {
                lastLine = lastLine.substring(0, lastBraceIndex);
            }
            int secondLastLineIndex = fileContent.lastIndexOf(System.lineSeparator(), fileContent.lastIndexOf(System.lineSeparator()) - 1);

            String newContent1 = "\n    @GetMapping(\"/" + businessName + "/" + businessName + ".html\")\n";
            newContent1 += "    public String " + businessName + "() {\n";
            newContent1 += "        return \"" + businessName + "/" + businessName + "\";\n";
            newContent1 += "    }\n";

            String newContent = "\n    @GetMapping(\"/" + businessName + "/" + businessName + "-add-update.html\")\n";
            newContent += "    public String " + businessName + "AddUpdate() {\n";
            newContent += "        return \"" + businessName + "/" + businessName + "-add-update\";\n";
            newContent += "    }\n";

            fileContent.insert(secondLastLineIndex, newContent);
            fileContent.insert(secondLastLineIndex, newContent1);
            FileWriter writer = new FileWriter(file);
            writer.write(fileContent.toString());
            writer.write("}");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
