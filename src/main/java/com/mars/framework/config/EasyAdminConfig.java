package com.mars.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author mars
 */
@Data
@Component
@ConfigurationProperties(prefix = "easy.admin")
public class EasyAdminConfig {

    /**
     * token过期时间
     */
    private Integer tokenExpireTime;


}
