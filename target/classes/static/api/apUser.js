/**
 * 分页查询用户列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apUser/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询用户详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apUser/query/' + id,
        method: 'get'
    })
}

/**
 * 新增用户
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apUser/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改用户
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apUser/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除用户
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apUser/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出用户
 *
 * @param query
 * @returns {*}
 */
function exportUser(query) {
    return requests({
        url: '/admin/apUser/export',
        method: 'get',
        params: query
    })
}
