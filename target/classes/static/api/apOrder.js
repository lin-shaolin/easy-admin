/**
 * 分页查询订单列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apOrder/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询订单详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apOrder/query/' + id,
        method: 'get'
    })
}

/**
 * 新增订单
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apOrder/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改订单
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apOrder/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除订单
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apOrder/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出订单
 *
 * @param query
 * @returns {*}
 */
function exportOrder(query) {
    return requests({
        url: '/admin/apOrder/export',
        method: 'get',
        params: query
    })
}
