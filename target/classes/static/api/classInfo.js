/**
 * 分页查询班级列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/classInfo/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询班级详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/classInfo/query/' + id,
        method: 'get'
    })
}

/**
 * 新增班级
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/classInfo/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改班级
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/classInfo/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除班级
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/classInfo/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出班级
 *
 * @param query
 * @returns {*}
 */
function exportInfo(query) {
    return requests({
        url: '/admin/classInfo/export',
        method: 'get',
        params: query
    })
}
